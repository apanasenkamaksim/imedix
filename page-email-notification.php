<?php get_header(); ?>

<main>
	<div class="container">
		<div class="row">
			<aside class="col-lg-3 sidebar-settings d-none d-lg-block">

				<?php include get_template_directory() . '/templates/settings_sidebar.php'; ?>

			</aside>
			<div class="col-lg-9">
				<div class="content-settings email_notification">
					<h1 class="page_title">
						Email Notification Settings
					</h1>

					<?php $options = get_notification_settings(); ?>
					<form action="" id="ajax_email_notification_settings" novalidate>
						<div class="row">
							<div class="col-12 option">
								<label class="checkbox">Imedix Digest
								  <input type="checkbox" name="new_question_notification" value="1" <?php if ( $options['new_question_notification'] ) echo 'checked'; ?>>
								  <span class="checkmark"></span>
								</label>
								<div class="description">
									Imedix Digest emails consist of Top questions and other popular content from your feed.
								</div>
								<div class="radio_btns">
									<div class="row">
										<div class="col-sm-6">
											<label class="radio">Up to once a day
											  <input type="radio" name="question_notification" value="daily" <?php if ( $options['question_notification'] == 'daily' ) echo 'checked'; ?>>
											  <span class="checkmark"></span>
											</label>
										</div>
										<div class="col-sm-6">
											<label class="radio">Weekly
											  <input type="radio" name="question_notification" value="weekly" <?php if ( $options['question_notification'] == 'weekly' ) echo 'checked'; ?>>
											  <span class="checkmark"></span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 option">
								<label class="checkbox">New Followers
								  <input type="checkbox" name="new_follower_notification" value="1" <?php if ( $options['new_follower_notification'] ) echo 'checked'; ?>>
								  <span class="checkmark"></span>
								</label>
								<div class="description">
									We’ll email you when a new person starts following you.
								</div>
							</div>
							<div class="col-12 option">
								<label class="checkbox">Answers
								  <input type="checkbox" name="new_answer_notification" value="1" <?php if ( $options['new_answer_notification'] ) echo 'checked'; ?>>
								  <span class="checkmark"></span>
								</label>
								<div class="description">
									We’ll email you when someone answers on yours questions.
								</div>
							</div>
							<div class="col-12 option">
								<label class="checkbox">Comments
								  <input type="checkbox" name="new_comment_notification" value="1" <?php if ( $options['new_comment_notification'] ) echo 'checked'; ?>>
								  <span class="checkmark"></span>
								</label>
								<div class="description">
									We’ll email you when someone coments on yours answers.
								</div>
							</div>
							<div class="col-12 option">
								<label class="checkbox">Upvotes
								  <input type="checkbox" name="new_upvote_notification" value="1" <?php if ( $options['new_upvote_notification'] ) echo 'checked'; ?>>
								  <span class="checkmark"></span>
								</label>
								<div class="description">
									We’ll email you when someone upwotes on yours answers.
								</div>
							</div>
							<div class="col-12 text-center">
								<?php stl_wp_nonce_field('ajax-email-notification-settings', 'security'); ?>
								<input type="submit" class="blue_btn" value="Save">
							</div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
