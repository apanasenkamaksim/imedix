<?php
add_action( 'wp_ajax_ajax_comment_votes', 'ajax_comment_votes' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_comment_votes', 'ajax_comment_votes' ); // For anonymous users

function ajax_comment_votes(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-comment-votes', 'security' );

  if( empty( $_POST[ 'submit' ] ) || empty( $_POST[ 'comment_id' ] ) ) {
  	echo json_encode( array( 'state'=>false ) );
  	die();
  }

	$key = ( $_POST[ 'submit'] == 'up' ) ? 'upvotes' : 'downvotes';
	$field = ( $_POST[ 'submit'] == 'up' ) ? 'comment_upvote_' : 'comment_downvote_';
  $comment_id = $_POST[ 'comment_id' ];

 	$user_id = get_current_user_id();
	if( empty( $user_id ) ) {
  	echo json_encode( array( 'state'=>false ) );
  	die();
	}

	$set_comment_votes = get_user_meta( $user_id, 'comment_upvote_' . $comment_id, true ) || get_user_meta( $user_id, 'comment_downvote_' . $comment_id, true );

	if( $set_comment_votes ) {
  	echo json_encode( array( 'state'=>false, 'message'=>__('You already gave vote for this comment.', 'imedix') ) );
  	die();
	}

	$value = get_field( $key, 'comment_' . $comment_id );
	$value++;
	update_field( $key, $value, 'comment_' . $comment_id );
	update_user_meta( $user_id, $field . $comment_id, true );

	do_action( 'upvote_or_downvote_was_set', $user_id, $comment_id );
	if ( $_POST[ 'submit'] == 'up' ) {
		do_action( 'upvote_was_set', $user_id, $comment_id );
	} else {
		do_action( 'downvote_was_set', $user_id, $comment_id );
	}

	$upvotes = get_field( 'upvotes', 'comment_' . $comment_id );
	$downvotes = get_field( 'downvotes', 'comment_' . $comment_id );

  echo json_encode( array( 'state'=>true, 'votes' => $upvotes - $downvotes ) );
  die();
}
