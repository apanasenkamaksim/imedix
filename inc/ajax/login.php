<?php
// add_action( 'wp_ajax_ajax_login', 'ajax_login' ); // For logged in users
add_action( 'wp_ajax_nopriv_ajax_login', 'ajax_login' ); // For anonymous users

function ajax_login(){
  // First check the nonce, if it fails the function will break
  check_ajax_referer( 'ajax-login', 'security' );

  // Nonce is checked, get the POST data and sign user on
	// Call auth_user_login
	$answer = auth_user_login($_POST['email'], $_POST['password'], __('Login', 'imedix'));

	echo json_encode($answer);

  die();
}

function auth_user_login($user_login, $password, $login) {
	$info = array();
  $info['user_login'] = $user_login;
  $info['user_password'] = $password;
  $info['remember'] = true;

	$user_signon = wp_signon( $info, false );
  if ( is_wp_error($user_signon) ) {
  	$message = $user_signon->get_error_code() == 'email_is_not_confirmed' ? $user_signon->get_error_message() : 'Wrong login or password';
		return array('loggedin'=>false, 'message'=>$message);
  } else {
		wp_set_current_user($user_signon->ID);
    return array('loggedin'=>true, 'message'=>$login . __(' successful, redirecting...', 'imedix'));
  }
}
