<?php
add_action( 'wp_ajax_ajax_add_question_to_favorites', 'ajax_add_question_to_favorites' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_add_question_to_favorites', 'ajax_add_question_to_favorites' ); // For anonymous users

function ajax_add_question_to_favorites(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'category_ID' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_add_question_to_favorites') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
		die();
	}

	$user_id = get_current_user_id();
	if( empty( $user_id ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
		die();
	}

	$term_id = $_POST[ 'category_ID' ];


	if ( ! usermeta_value_exists( $user_id, 'favorite_categories', $term_id ) ) {
		$meta_key = add_user_meta( $user_id, 'favorite_categories', $term_id );

		$count_key = 'followers';
		$count = (int) get_term_meta( $term_id, $count_key, true );
	  $count++;
	  update_term_meta( $term_id, $count_key, $count );
	} else {
		$meta_key = true;
	}

	if( $meta_key ) {
		$favorites_menu = display_favorites_menu( 'category', $term_id, false );
		$favorites_button = display_favorites_button( 'category', $term_id, false );
		echo json_encode( array( 'state'=>true, 'favorites_menu'=>$favorites_menu, 'favorites_button'=>$favorites_button ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
	}

  die();
}
