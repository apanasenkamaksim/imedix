<?php
add_action( 'wp_ajax_ajax_get_question', 'ajax_get_question' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_get_question', 'ajax_get_question' ); // For anonymous users

function ajax_get_question(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'post_id' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_get_question') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t get post.' ) );
		die();
	}


	$post = get_post( $_POST[ 'post_id' ] );

	if( $post ) {
		echo json_encode( array( 'state'=>true, 'post_id'=>$post->ID, 'title'=>$post->post_title, 'categories'=>wp_get_post_categories( $post->ID ), 'content'=>$post->post_content ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t get post.' ) );
	}

  die();
}
