<?php
add_action( 'wp_ajax_ajax_add_user_to_favorites', 'ajax_add_user_to_favorites' ); // For logged in users
// add_action( 'wp_ajax_nopriv_ajax_add_user_to_favorites', 'ajax_add_user_to_favorites' ); // For anonymous users

function ajax_add_user_to_favorites(){
  // First check the nonce, if it fails the function will break

	if( empty( $_POST[ 'user_id' ] ) || empty( $_POST[ 'nonce' ] ) || ! wp_verify_nonce( $_POST[ 'nonce' ], 'ajax_add_user_to_favorites') ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
		die();
	}

	$current_user_id = get_current_user_id();
	if( empty( $current_user_id ) ) {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
		die();
	}

	$user_id = $_POST[ 'user_id' ];

	$count = (int) get_user_meta( $current_user_id, $count_key, true );

	if ( ! usermeta_value_exists( $current_user_id, 'following', $user_id ) ) {
		$meta_key = add_user_meta( $current_user_id, 'following', $user_id );

		$count_key = 'following_count';
		$count = (int) get_user_meta( $current_user_id, $count_key, true );
        $count++;
        update_user_meta( $current_user_id, $count_key, $count );
	} else {
		$meta_key = true;
	}

	if ( ! usermeta_value_exists( $user_id, 'followers', $current_user_id ) ) {
		$meta_key = add_user_meta( $user_id, 'followers', $current_user_id );

		$count_key = 'followers_count';
		$count = (int) get_user_meta( $user_id, $count_key, true );
        $count++;
        update_user_meta( $user_id, $count_key, $count );
	} else {
		$meta_key = true;
	}

	if( $meta_key ) {

		do_action( 'follower_added', $current_user_id, $user_id );

		$favorites_menu = display_favorites_menu( 'user', $user_id, false );
		$favorites_small_button = display_favorites_small_button( 'user', $user_id, false );
		$favorites_button = display_favorites_button( 'user', $user_id, false );
		echo json_encode( array( 'state'=>true, 'favorites_menu'=>$favorites_menu, 'favorites_small_button'=>$favorites_small_button, 'favorites_button'=>$favorites_button ) );
	} else {
		echo json_encode( array( 'state'=>false, 'message'=>'Can\'t add to favorites.' ) );
	}

  die();
}
