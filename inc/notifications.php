<?php

function get_notification_settings( $user_id = false ) {
	$default = array(
		'new_question_notification' => true,
		'question_notification' => 'weekly',
		'new_follower_notification' => true,
		'new_answer_notification' => true,
		'new_comment_notification' => true,
		'new_upvote_notification' => true,

	);

	if ( ! $user_id ) $user_id = get_current_user_id();
	$options = get_user_meta( $user_id, 'notification_settings', true );
	if ( empty( $options ) ) $options = array();

	$options = array_merge( $default, $options );

	return $options;
}

function create_notification_database_table() {
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();

	$table_name = $wpdb->prefix . 'notifications';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
	  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	  `user_action_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	  `comment_action_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	  `post_action_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
	  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
	  `date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
		`action` varchar(255) NOT NULL DEFAULT '',
		`readed` int(10) NOT NULL DEFAULT '0',
		`email_sent` int(10) NOT NULL DEFAULT '0',
		PRIMARY KEY (`ID`)
	) $charset_collate;";

	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	dbDelta( $sql );
}

function get_notifications( $posts_per_page = false ) {
	create_notification_database_table();

	$user_id = get_current_user_id();
	if ( empty( $user_id ) ) return false;

	global $wpdb;

	$results = new stdClass();
	if ( !$posts_per_page )
		$posts_per_page = get_option( 'posts_per_page' );
	$paged = is_page( 'notifications' ) && get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$query = "SELECT SQL_CALC_FOUND_ROWS * FROM " . $wpdb->prefix . "notifications WHERE user_id='" . $user_id . "' ORDER BY ID DESC LIMIT " . $posts_per_page . " OFFSET " . ( $paged - 1 ) * $posts_per_page;
	$results->notifications  = $wpdb->get_results( $query );
	$results->found_notifications  = $wpdb->get_var( "SELECT FOUND_ROWS()" );
	$results->notifications_count  = count( $results->notifications );
	$results->max_num_pages  = ceil( $results->found_notifications / $posts_per_page );
	return $results ;
}

function notification_readed( $id ) {
    global $wpdb;

    $table_name = $wpdb->base_prefix . 'notifications';

    $rows = $wpdb->update( $table_name,
        array( 'readed' => 1 ),
        array( 'ID' => $id ),
        array( '%d' ),
        array( '%d' )
    );

    if ( $rows ) {
        return true;
    } else {
        return false;
    }
}

function new_answer_or_comment_notification( $comment_action_id, $comment ) {
	global $wpdb;

	$action = $comment->comment_parent == 0 ? 'answer' : 'comment';

	$user_id = $comment->comment_parent == 0 ? get_post_field( 'post_author', $comment->comment_post_ID ) : get_comment($comment->comment_parent)->user_id;
	$user_action_id = $comment->user_id;
	$post_action_id = $comment->comment_post_ID;

	$table_name = $wpdb->prefix . 'notifications';
	$wpdb->insert(
		$table_name,
		array(
			'date' => current_time( 'mysql' ),
			'date_gmt' => current_time( 'mysql', 1 ),
			'user_id' => $user_id,
			'user_action_id' => $user_action_id,
			'comment_action_id' => $comment_action_id,
			'post_action_id' => $post_action_id,
			'action' => $action,
			'readed' => 0,
			'email_sent' => 1
		)
	);

	$notification_settings = get_notification_settings( $user_id );
	if ( ( $comment->comment_parent == 0 && $notification_settings['new_answer_notification'] ) || ( $comment->comment_parent != 0 && $notification_settings['new_comment_notification'] ) ) {

		add_filter( 'wp_mail_content_type', 'set_html_content_type' );
		$user = get_user_by( 'ID', $user_id );
		$recipients = $user->user_email;
		$user_action = get_user_by( 'ID', $user_action_id );
		$href = get_post_field( 'guid', $post_action_id ) . '#comment-' . $comment_action_id;
		$word = $comment->comment_parent == 0 ? 'answered' : 'commented';
		$post_action_title = get_post_field( 'post_title', $post_action_id );
		$text = '<a href="' . $href . '"><b>' . $user_action->display_name . '</b> ' . $word . ' in your topic <i>“' . $post_action_title . '”</i></a>';
		wp_mail( $recipients, 'Notification from iMEDix.com', $text );
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

	}
}
add_action( 'wp_insert_comment', 'new_answer_or_comment_notification', 10, 2 );

function new_upvote_notification( $user_action_id, $comment_action_id ) {
	global $wpdb;

	$comment = get_comment( $comment_action_id );
	$action = 'upvote';
	$user_id = $comment->user_id;
	$comment_action_id = $comment->comment_ID;
	$post_action_id = $comment->comment_post_ID;

	$table_name = $wpdb->prefix . 'notifications';
	$wpdb->insert(
		$table_name,
		array(
			'date' => current_time( 'mysql' ),
			'date_gmt' => current_time( 'mysql', 1 ),
			'user_id' => $user_id,
			'user_action_id' => $user_action_id,
			'comment_action_id' => $comment_action_id,
			'post_action_id' => $post_action_id,
			'action' => $action,
			'readed' => 0,
			'email_sent' => 1
		)
	);

	$notification_settings = get_notification_settings( $user_id );
	if ( $notification_settings['new_upvote_notification'] ) {

		add_filter( 'wp_mail_contepost_action_titlent_type', 'set_html_content_type' );
		$user = get_user_by( 'ID', $user_id );
		$recipients = $user->user_email;
		$user_action = get_user_by( 'ID', $user_action_id );
		$href = get_post_field( 'guid', $post_action_id ) . '#comment-' . $comment_action_id;
		$word = $comment->comment_parent == 0 ? 'answer' : 'comment';
		$post_action_title = get_post_field( 'post_title', $post_action_id );
		$text = '<a href="' . $href . '"><b>' . $user_action->display_name . '</b> upvoted your ' . $word . ' in topic <i>“' . $post_action_title . '”</i></a>';
		wp_mail( $recipients, 'Notification from iMEDix.com', $text );
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

	}
}
add_action( 'upvote_was_set', 'new_upvote_notification', 10, 2 );

function new_follower_notification( $user_action_id, $user_id ) {
	global $wpdb;

	$action = 'follower';

	$table_name = $wpdb->prefix . 'notifications';
	$wpdb->insert(
		$table_name,
		array(
			'date' => current_time( 'mysql' ),
			'date_gmt' => current_time( 'mysql', 1 ),
			'user_id' => $user_id,
			'user_action_id' => $user_action_id,
			'action' => $action,
			'readed' => 0,
			'email_sent' => 1
		)
	);

	$notification_settings = get_notification_settings( $user_id );
	if ( $notification_settings['new_follower_notification'] ) {

		add_filter( 'wp_mail_contepost_action_titlent_type', 'set_html_content_type' );
		$user = get_user_by( 'ID', $user_id );
		$recipients = $user->user_email;
		$user_action = get_user_by( 'ID', $user_action_id );
		$href = get_author_posts_url($user_action_id);
		$text = '<a href="' . $href . '"><b>' . $user_action->display_name . '</b> follow you</a>';
		wp_mail( $recipients, 'Notification from iMEDix.com', $text );
		remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

	}
}
add_action( 'follower_added', 'new_follower_notification', 10, 2 );
