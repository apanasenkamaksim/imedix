<?php

function d($var) {
    if(empty($var))
        return;
    die(var_dump($var));
}

function f($var) {
    if(empty($var))
        return;
    global $wp_filter;
    die(var_dump($wp_filter[$var]));
}

function get_domain() {
    $domain = home_url();
    preg_match('/^(?:https?:\/\/)?(?:www\.)?([^:\/?\n]+)/', $domain, $match);
    $domain = $match[1];

    return $domain;
}

function stl_wp_nonce_field($action = -1, $name = "_wpnonce", $referer = true, $echo = true) {
    $name        = esc_attr($name);
    $nonce_field = '<input type="hidden" name="' . $name . '" value="' . wp_create_nonce($action) . '" />';

    if($referer)
        $nonce_field .= wp_referer_field(false);

    if($echo)
        echo $nonce_field;

    return $nonce_field;
}

function current_url() {
    global $wp;
    $current_slug = add_query_arg(array(), $wp->request);

    return home_url($current_slug);
}

function usermeta_value_exists($user_id, $meta_key, $meta_value) {
    if(empty($user_id))
        $user_id = get_current_user_id();
    if(empty($user_id) || empty($meta_key) || empty($meta_value))
        return false;

    global $wpdb;

    $test = $wpdb->get_results("SELECT meta_key FROM " . $wpdb->prefix . "usermeta where user_id='" . $user_id . "' AND meta_key='" . $meta_key . "' AND meta_value='" . $meta_value . "'");
    if(($wpdb->num_rows) > 0) {
        // custom field exists
        return true;
    } else {
        // field does not exist
        return false;
    }
}

function get_usermeta_values($user_id, $meta_key, $posts_per_page = false) {
    if(empty($user_id))
        $user_id = get_current_user_id();
    if(empty($user_id) || empty($meta_key))
        return false;

    global $wpdb;

    $results = new stdClass();
    if(!$posts_per_page)
        $posts_per_page = get_option('posts_per_page');
    $paged                  = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $results->meta          = $wpdb->get_results("SELECT SQL_CALC_FOUND_ROWS * FROM " . $wpdb->prefix . "usermeta WHERE user_id='" . $user_id . "' AND meta_key='" . $meta_key . "' LIMIT " . $posts_per_page . " OFFSET " . ($paged - 1) * $posts_per_page);
    $results->found_meta    = $wpdb->get_var("SELECT FOUND_ROWS()");
    $results->meta_count    = count($results->meta);
    $results->max_num_pages = ceil($results->found_meta / $posts_per_page);
    return $results;
}

function isCurrentUserConnected($dbID) {
    global $wpdb;

    $current_user = wp_get_current_user();
    $ID           = $wpdb->get_var($wpdb->prepare('SELECT identifier FROM `' . $wpdb->prefix . 'social_users` WHERE type LIKE %s AND ID = %d', array($dbID,
                                                                                                                                                     $current_user->ID
    )));
    if($ID === null) {
        return false;
    }

    return $ID;
}

function get_current_category_id() {
    if(!is_category())
        return false;
    $category = get_queried_object();
    return $category->term_id;
}

function is_category_post($slug, $post_id = false) {
    if(!is_single())
        return false;

    if($post_id) {
        $post = get_post($post_id);
    } else {
        $post = get_post();
    }

    if(!empty($post)) {
        if(in_category($slug, $post))
            return true;

        $term             = get_term_by('slug', $slug, 'category');
        $child_categories = get_term_children($term->term_id, 'category');

        if(in_category($child_categories, $post))
            return true;
    }

    return false;
}

function is_needed_category($slug) {
    if(is_category()) {
        if(is_category($slug))
            return true;

        $term             = get_term_by('slug', $slug, 'category');
        $child_categories = get_term_children($term->term_id, 'category');

        $category = get_queried_object();
        if(in_array($category->term_id, $child_categories))
            return true;
    }

    return false;
}

function is_needed_page_by_slug($slug) {
    if(is_page()) {
        if(is_page($slug))
            return true;

        $page = get_page_by_path($slug);

        if(!empty($page) && is_needed_page_by_id($page->ID))
            return true;
    }

    return false;
}

function is_needed_page_by_id($pid, $cpid = false) {
    if(!is_page())
        return false;
    if(!$cpid) {
        global $post;
        $current_page = $post;
    } else {
        $current_page = get_post($cpid);
    }
    if(empty($current_page))
        return false;
    if($current_page->ID == $pid)
        return true;
    if($current_page->post_parent == 0)
        return false;
    if($current_page->post_parent == $pid || is_needed_page_by_id($pid, $current_page->post_parent))
        return true; else
        return false;
}

;

function stl_get_comment_depth($comment_id) {
    $depth_level = 0;
    while ($comment_id > 0) {
        $comment    = get_comment($comment_id);
        $comment_id = $comment->comment_parent;
        $depth_level++;
    }
    return $depth_level;
}

function display_favorites_menu($type = '', $ID = false, $echo = true) {
    // if ( ! is_user_logged_in() ) return false;
    switch ($type) {
        case 'category':
            if(!$ID && !is_category())
                return false;
            if(!$ID)
                $ID = get_current_category_id();
            $description = category_description($ID);
            if(is_user_logged_in()) {
                $add_favorites = !usermeta_value_exists(get_current_user_id(), 'favorite_categories', $ID);
                $link_class    = $add_favorites ? 'add_question_to_favorites_link' : 'remove_question_from_favorites_link';
                $data_nonce    = $add_favorites ? wp_create_nonce('ajax_add_question_to_favorites') : wp_create_nonce('ajax_remove_question_from_favorites');
            } else {
                $add_favorites = true;
                $link_class    = 'registration_popup_link';
                $data_nonce    = '';
            }
            $count_text = 'Questions';
            $category   = get_category($ID);
            $count      = $category->category_count;
            break;

        case 'user':
            if(!$ID && !is_page('user'))
                return false;
            if(!$ID)
                $ID = get_query_var('userid');
            if(get_current_user_id() == $ID)
                return false;
            if(is_user_logged_in()) {
                $add_favorites = !usermeta_value_exists(get_current_user_id(), 'following', $ID);
                $link_class    = $add_favorites ? 'add_user_to_favorites_link' : 'remove_user_from_favorites_link';
                $data_nonce    = $add_favorites ? wp_create_nonce('ajax_add_user_to_favorites') : wp_create_nonce('ajax_remove_user_from_favorites');
            } else {
                $add_favorites = true;
                $link_class    = 'registration_popup_link';
                $data_nonce    = '';
            }
            $count_text = 'Followers';
            $count_key  = 'followers_count';
            $count      = (int)get_user_meta($ID, $count_key, true);
            break;

        default:
            return false;
            break;
    }

    if(!$echo)
        ob_start();
    include get_template_directory() . '/templates/favorites_menu.php';
    if(!$echo)
        $favorites_menu = ob_get_clean();
    if(!$echo)
        return $favorites_menu;
}

function display_favorites_small_button($type = '', $ID = false, $echo = true) {
    if(!is_user_logged_in())
        return false;
    switch ($type) {
        case 'user':
            if(!$ID && !is_page('user'))
                return false;
            if(!$ID)
                $ID = get_query_var('userid');
            if(get_current_user_id() == $ID)
                return false;
            $add_favorites = !usermeta_value_exists(get_current_user_id(), 'following', $ID);
            $link_class    = $add_favorites ? 'add_user_to_favorites_link' : 'remove_user_from_favorites_link';
            $data_nonce    = $add_favorites ? wp_create_nonce('ajax_add_user_to_favorites') : wp_create_nonce('ajax_remove_user_from_favorites');
            break;

        default:
            return false;
            break;
    }

    if(!$echo)
        ob_start();
    include get_template_directory() . '/templates/favorites_small_button.php';
    if(!$echo)
        $favorites_small_button = ob_get_clean();
    if(!$echo)
        return $favorites_small_button;
}

function display_favorites_button($type = '', $ID = false, $echo = true) {
    if(!is_user_logged_in())
        return false;
    switch ($type) {
        case 'category':
            if(!$ID && !is_category())
                return false;
            if(!$ID)
                $ID = get_current_category_id();
            $add_favorites = !usermeta_value_exists(get_current_user_id(), 'favorite_categories', $ID);
            $link_class    = $add_favorites ? 'add_question_to_favorites_link' : 'remove_question_from_favorites_link';
            $data_nonce    = $add_favorites ? wp_create_nonce('ajax_add_question_to_favorites') : wp_create_nonce('ajax_remove_question_from_favorites');
            break;

        case 'user':
            if(!$ID && !is_page('user'))
                return false;
            if(!$ID)
                $ID = get_query_var('userid');
            if(get_current_user_id() == $ID)
                return false;
            $add_favorites = !usermeta_value_exists(get_current_user_id(), 'following', $ID);
            $link_class    = $add_favorites ? 'add_user_to_favorites_link' : 'remove_user_from_favorites_link';
            $data_nonce    = $add_favorites ? wp_create_nonce('ajax_add_user_to_favorites') : wp_create_nonce('ajax_remove_user_from_favorites');
            break;

        default:
            return false;
            break;
    }

    if(!$echo)
        ob_start();
    include get_template_directory() . '/templates/favorites_button.php';
    if(!$echo)
        $favorites_button = ob_get_clean();
    if(!$echo)
        return $favorites_button;
}

function time_elapsed_string($datetime, $full = false) {
    $now  = new DateTime;
    $ago  = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array('y' => 'year',
                    'm' => 'month',
                    'w' => 'week',
                    'd' => 'day',
                    'h' => 'hour',
                    'i' => 'minute',
                    's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if(!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function set_html_content_type() {
    return 'text/html';
}

function display_rating_inputs() {
    ob_start();

    include get_template_directory() . '/templates/rating_inputs.php';

    $content = ob_get_clean();
    return $content;
}

function display_rating($rating, $microdata = false) {
    ?>
    <div id="display_rating">
        <?php for ($i = 1; $i <= min(5, $rating); $i++) : ?>
        <span class="checked"></span>
        <?php endfor; ?>
        <?php for ($i = 0; $i < 5 - min(5, $rating); $i++) : ?>
        <span></span>
        <?php endfor; ?>
    </div>
    <?php if($microdata) : ?>
        <span>
            <meta content="1">
            <meta content="<?php echo $rating; ?>">
            <meta content="5">
            <meta content="1">
        </span>
    <?php endif; ?>
    <?php
}
