<?php

/**
 * Adding sidebars.
 */
function short_register_sidebar( $name, $id, $class = '' ) {
	register_sidebar( array(
		'name'          => $name,
		'id'            => $id,
		'description'   => '',
		'class'         => 'sidebar ' . $class,
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => "</div>\n",
		'before_title'  => '<h2 class="widget_title">',
		'after_title'   => "</h2>\n",
	) );
}
function register_custom_sidebars(){
	short_register_sidebar( 'Front page', 'front_page_sidebar');
	short_register_sidebar( 'Front page content', 'front_page_content');
	short_register_sidebar( 'Front page content 2', 'front_page_content_2');

	short_register_sidebar( 'Question post content', 'question_post_content');
	short_register_sidebar( 'Question post sidebar', 'question_post_sidebar');
	short_register_sidebar( 'Question category content', 'question_category_content');
	short_register_sidebar( 'Question category content 2', 'question_category_content_2');
	short_register_sidebar( 'Question category sidebar', 'question_category_sidebar');

	short_register_sidebar( 'Search page content', 'search_page_content');
	short_register_sidebar( 'Search page content 2', 'search_page_content_2');
	short_register_sidebar( 'Search page sidebar', 'search_page_sidebar');

	short_register_sidebar( 'Drugs post content', 'drugs_post_content');
	short_register_sidebar( 'Drugs post sidebar', 'drugs_post_sidebar');
	short_register_sidebar( 'Drugs category content', 'drugs_category_content');
	short_register_sidebar( 'Drugs category content 2', 'drugs_category_content_2');
	short_register_sidebar( 'Drugs category sidebar', 'drugs_category_sidebar');

	short_register_sidebar( 'Pharmacy post content', 'pharmacy_post_content');
	short_register_sidebar( 'Pharmacy post sidebar', 'pharmacy_post_sidebar');
	short_register_sidebar( 'Pharmacy category sidebar', 'pharmacy_category_sidebar');

	short_register_sidebar( 'Users post sidebar', 'user_post_sidebar');
	short_register_sidebar( 'Users category sidebar', 'user_category_sidebar');

	short_register_sidebar( 'Footer 1', 'footer_1_sidebar');
	short_register_sidebar( 'Footer 2', 'footer_2_sidebar');
	short_register_sidebar( 'Footer 3', 'footer_3_sidebar');
	short_register_sidebar( 'Footer 4', 'footer_4_sidebar');

    short_register_sidebar( 'Post sidebar', 'post_sidebar');
}
add_action( 'widgets_init', 'register_custom_sidebars' );
