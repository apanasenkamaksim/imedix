<?php
  function display_pagination($paginate) {
    if(!is_array($paginate)) return;

    $pages_html = '';

    $prev_link = '<div class="navigation_item prev disable"><span class="prev page-numbers"><i class="fas fa-caret-left"></i></span></div>';
    $next_link = '<div class="navigation_item next disable"><span class="next page-numbers"><i class="fas fa-caret-right"></i></span></div>';

    if (preg_match('/current/', $paginate[0])) {
    	$first_link = '<div class="navigation_item first disable"><span class="first page-numbers"><i class="fas fa-backward"></i></span></div>';
    } else {
      $first_link = '<div class="navigation_item first">' . preg_replace('/(<a[^>]*>).*?(<\/a>)/i','$1<i class="fas fa-backward"></i>$2',$paginate[1]) . '</div>';
    }
    if (preg_match('/current/', $paginate[count($paginate)-1])) {
    	$last_link = '<div class="navigation_item last disable"><span class="last page-numbers"><i class="fas fa-forward"></i></span></div>';
    } else {
      $last_link = '<div class="navigation_item last">' . preg_replace('/(<a[^>]*>).*?(<\/a>)/i','$1<i class="fas fa-forward"></i>$2',$paginate[count($paginate)-2]) . '</div>';
    }

    foreach ($paginate as $index => $page) {
      if (preg_match('/class="prev page-numbers"/', $page)) {
        $prev_link = '<div class="navigation_item prev">' . $page . '</div>';
        continue;
      }

      if (preg_match('/class="next page-numbers"/', $page)) {
        $next_link = '<div class="navigation_item next">' . $page . '</div>';
        continue;
      }

      if (preg_match('/current/', $page)) {
      	$pages_html .= '<div class="navigation_item current">' . $page . '</div>';
      	continue;
      }
      if (preg_match('/dots/', $page)) {
      	$pages_html .= '<div class="navigation_item dots">' . $page . '</div>';
      	continue;
      }

      $pages_html .= '<div class="navigation_item">' . $page . '</div>';
    }


    printf(
      '<div id="pagination_navigation">%s%s%s%s%s</div>',
      $first_link,
      $prev_link,
      $pages_html,
      $next_link,
      $last_link
    );
  }
