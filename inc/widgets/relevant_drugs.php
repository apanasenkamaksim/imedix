<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Relevant_Drugs_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'relevant_drugs', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - relevant drugs ***/',
			array( 'description' => 'Relevant drugs', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		$title = apply_filters( 'widget_title', get_field( 'title', 'widget_' . $widget_id ) );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		// Код виджета
		if ( ! is_single() ) return;
		if ( ! is_category_post( 'qa' ) ) return;
		$count = get_field( 'count', 'widget_' . $widget_id );
		$term = get_term_by( 'slug', 'drugs', 'category' );
		if ( $term ) $terms = $term->term_id;

		$terms = get_field( 'related_to_question_categories', get_the_ID() );

		$meta_query = array();

		foreach ( wp_get_post_categories( get_the_ID() ) as $term_id ) {
			$meta_query[] = array(
					'key' => 'related_to_question_categories',
					'value' => '"' . $term_id . '"',
					'compare' => 'LIKE'
				);
		}

		if ( is_array( $meta_query ) && count( $meta_query ) > 1 ) $meta_query['relation'] = 'OR';

		$options = array(
			'post_type' => 'post',
			'cat' => $term->term_id,
			'orderby' => array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' ),
			'meta_key' => 'post_rating',
			'posts_per_page' => $count,
			'meta_query' => $meta_query
		);
		$loop = new WP_Query( $options );
		// d( $loop );
		?>

		<?php if ( $loop->have_posts() ) : ?>
		<ul class="list-unstyled" id="relevant_drugs">
			<?php $i = 1; while ($loop->have_posts()) : $loop->the_post(); ?>
			<li class="item">
				<a href="<?php the_permalink(); ?>" class="item_link">
					<?php echo $i; ?>.	<?php the_title(); ?>
				</a>
			</li>
		<?php $i++; endwhile; wp_reset_query(); ?>
		</ul>
		<?php endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_relevant_drugs_widget() {
	register_widget( 'Relevant_Drugs_Widget' );
}
add_action( 'widgets_init', 'register_relevant_drugs_widget' );
