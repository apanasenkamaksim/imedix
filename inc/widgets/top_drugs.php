<?php
/**
 * Добавление нового виджета Foo_Widget.
 */
class Top_Drugs_Widget extends WP_Widget {

	// Регистрация виджета используя основной класс
	function __construct() {
		// вызов конструктора выглядит так:
		// __construct( $id_base, $name, $widget_options = array(), $control_options = array() )
		parent::__construct(
			'top_drugs', // ID виджета, если не указать (оставить ''), то ID будет равен названию класса в нижнем регистре
			'/*** STL - top drugs ***/',
			array( 'description' => 'Top drugs', /*'classname' => 'my_widget',*/ )
		);
	}

	/**
	 * Вывод виджета во Фронт-энде
	 *
	 * @param array $args     аргументы виджета.
	 * @param array $instance сохраненные данные из настроек
	 */
	function widget( $args, $instance ) {
		$widget_id = $args['widget_id'];

		$title = apply_filters( 'widget_title', get_field( 'title', 'widget_' . $widget_id ) );

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}


        $drugs = get_field( 'drugs', 'widget_' . $widget_id );

        global $post;
		?>

        <?php if ($drugs && count($drugs)) : ?>

		<ul class="list-unstyled" id="top_drugs">

            <?php foreach ($drugs as $drug) : $post = $drug['drug']; setup_postdata($post); ?>

			<li class="item">

                <div class="thumbnail">
                    <?php if(has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('pharmacy_reviews', array('class' => 'article_image')); ?>
                    <?php else : ?>
                        <img src="<?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail.png"
                             srcset="<?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail@2x.png 2x,
                                        <?php echo get_template_directory_uri(); ?>/production/images/pharmacy_reviews_thumbnail@3x.png 3x"
                             class="article_image">
                    <?php endif; ?>
                </div>
				
				<div class="text-center">
					<?php
						$verdict = get_field( 'verdict' );
						$temp = !empty( $verdict[ 'rating' ] ) ? $verdict[ 'rating' ] : 3;
						$temp = is_numeric( $temp ) && $temp > 0 && $temp <= 5 ? $temp : 3;
					?>
                    <?php display_rating($temp); ?>
				</div>
				<div class="title">
					<?php the_title(); ?>
				</div>
				<div class="text-center">
					<a href="<?php the_permalink(); ?>" class="blue_btn">
						View more
					</a>
				</div>
			</li>

        <?php endforeach; wp_reset_postdata(); ?>

		</ul>

		<a href="<?php echo home_url( '/category/pharmacy-reviews/' ); ?>" class="more_link">
			All  Reviews <i class="fas fa-arrow-right"></i>
		</a>
		<?php endif; ?>

		<?php

		echo $args['after_widget'];
	}

	/**
	 * Админ-часть виджета
	 *
	 * @param array $instance сохраненные данные из настроек
	 */
	function form( $instance ) {

		?>

		<?php
	}

	/**
	 * Сохранение настроек виджета. Здесь данные должны быть очищены и возвращены для сохранения их в базу данных.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance новые настройки
	 * @param array $old_instance предыдущие настройки
	 *
	 * @return array данные которые будут сохранены
	 */
	function update( $new_instance, $old_instance ) {
		// $instance = array();
		// $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

		return $new_instance;
	}

}
// конец класса Foo_Widget

// регистрация Foo_Widget в WordPress
function register_top_drugs_widget() {
	register_widget( 'Top_Drugs_Widget' );
}
add_action( 'widgets_init', 'register_top_drugs_widget' );
