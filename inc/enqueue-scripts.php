<?php

if(!defined('ABSPATH')) die;

/**
 * Enqueue scripts and styles.
 */
function theme_scripts() {
    if (!is_admin()) {
        // Deregister the jquery version bundled with WordPress.
        wp_deregister_script( 'jquery' );
        // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
        wp_enqueue_script( 'jquery', get_template_directory_uri() . '/production/js/jquery.min.js', array(), '3.3.1', false );
        // Deregister the jquery-migrate version bundled with WordPress.
        wp_deregister_script( 'jquery-migrate' );
        // CDN hosted jQuery migrate for compatibility with jQuery 3.x
        wp_register_script( 'jquery-migrate', '//code.jquery.com/jquery-migrate-3.0.1.min.js', array('jquery'), '3.0.1', false );
    }

    $user = wp_get_current_user();
    $allowed_roles = array('editor', 'administrator');
    if(!is_admin() || !array_intersect($allowed_roles, $user->roles)) {
        wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/production/css/app.min.css', array(), '' );

        wp_enqueue_script( 'theme-script', get_template_directory_uri() . '/production/js/app.min.js', array('jquery'), '', true );

        wp_localize_script( 'theme-script', 'theme', array(
            'url' => home_url(),
            'themeUrl' => get_template_directory_uri(),
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        ) );
    }
}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );
