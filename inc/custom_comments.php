<?php
if( ! is_admin() ) remove_all_filters( 'comment_form_field_comment', 999 );

function display_comment_form( $comment_parent_id = 0 ) {
	global $post;
	if( $post->post_status == 'pending' ) return;
	if( ! comments_open() || post_has_the_best_comment() ) {
		echo '<p class="comments_closed">' .  __( 'Comments are disabled for this question', 'imedix' ) . '</p>';
		return;
	}
	$avatar = get_avatar( wp_get_current_user()->user_email, '40' );
	// die(var_dump($avatar));
	// $avatar = '<img alt="" src="' . get_template_directory_uri() . '/production/images/user_thumbnail-40x40.png" class="avatar avatar-40 photo" height="40" width="40">';
	$args = array(
			'id_form' => '',
			'id_submit' => '',
			'title_reply' => '',
			'title_reply_before' => '',
			'title_reply_after' => '',
			'logged_in_as' => '',
			'must_log_in' => '<p class="must-log-in">' .  __( 'Please <a href="#" class="popup login_popup_link">login</a> or <a href="#" class="popup registration_popup_link">register</a> to post a comment', 'imedix' ) . '</p>',
			'class_form' => 'row comment-form',
			'submit_field' => '<p class="col-auto btn-col">%1$s %2$s</p>',
			'submit_button' => '<button name="%1$s" id="%2$s" class="%3$s">
				<img src="' . get_template_directory_uri() . '/production/images/send_icon.png"
						 srcset="' . get_template_directory_uri() . '/production/images/send_icon@2x.png 2x, ' .
								 get_template_directory_uri() . '/production/images/send_icon@3x.png 3x"
						 class="send_icon">
			</button>',
			'class_submit' => 'send_btn'
		);
        if((is_category_post( 'drugs' ) && $comment_parent_id == 0) || is_page('user')) {
            $args['comment_field'] = '<p class="col-auto avatar-col">' . $avatar . '</p><p class="col textarea-col">' . display_rating_inputs() . '<textarea id="comment" name="comment" rows="1"  aria-required="true" required="required" placeholder="Write answer"></textarea></p>';
        } else {
            $args['comment_field'] = '<p class="col-auto avatar-col">' . $avatar . '</p><p class="col textarea-col"><textarea id="comment" name="comment" rows="1"  aria-required="true" required="required" placeholder="Write answer"></textarea></p>';
        }

	global $pass_comment_parent_id;
	$pass_comment_parent_id = $comment_parent_id;

	add_filter( 'comment_id_fields', 'custom_get_comment_id_fields', 10, 3 );
	comment_form( $args );
}

function custom_get_comment_id_fields( $result, $id = 0, $replytoid  ) {
	if ( empty( $id ) )
		$id = get_the_ID();

	global $pass_comment_parent_id;
	$replytoid = $pass_comment_parent_id;

	$result  = "<input type='hidden' name='comment_post_ID' value='$id' id='comment_post_ID' />\n";
	$result .= "<input type='hidden' name='comment_parent' id='comment_parent' value='$replytoid' />\n";
	$result .= stl_wp_nonce_field('ajax-edit-comment', 'security', true, false) . "\n";

	return $result;
}

function post_has_the_best_comment( $post_id = false ) {
		if( ! $post_id ) {
			global $post;
			$post_id = $post->ID;
		}
		$args = array(
			'post_id' => $post_id,
			'parent' => 0,
		);

		if( $comments = get_comments( $args ) ){
			foreach( $comments as $comment ){
				$checked = get_field( 'best_answer_checked', 'comment_' . $comment->comment_ID );
				$best_answer = get_field( 'is_best_answer', 'comment_' . $comment->comment_ID );

				if( $checked && $best_answer ) return true;
			}
		}

		return false;
}

function custom_html5_comment( $comment, $args, $depth ) {
	$tag = ( 'div' === $args['style'] ) ? 'div' : 'li';
?>
	<?php
		$checked = get_field( 'best_answer_checked', 'comment_' . get_comment_ID() );
		$best_answer = get_field( 'is_best_answer', 'comment_' . get_comment_ID() );
	?>
	<<?php echo $tag; ?> id="comment-<?php comment_ID(); ?>" <?php comment_class( $args['has_children'] ? 'parent list-unstyled' : 'list-unstyled', $comment ); ?>>
		<?php if (is_category_post( 'qa' )): ?>
			<article id="div-comment-<?php comment_ID(); ?>"<?php echo ($checked && $best_answer) ? ' itemprop="acceptedAnswer"' : ' itemprop="suggestedAnswer"'; ?> itemscope itemtype="https://schema.org/Answer" class="comment-body">
		<?php else: ?>
			<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
		<?php endif; ?>
			<div class="row">
				<div class="col-auto info-col">
					<?php if( is_category_post( 'qa' ) ) : ?>
					<div class="upvotes">
						<form action="" class="set_votes">
							<?php wp_nonce_field('ajax-comment-votes', 'security'); ?>
							<input type="hidden" name="comment_id" value="<?php comment_ID(); ?>">
							<?php
								$class = is_user_logged_in() ? 'enabled' : 'disabled';
								$upvotes = (int) get_field( 'upvotes', 'comment_' . get_comment_ID() );
								$downvotes = (int) get_field( 'downvotes', 'comment_' . get_comment_ID() );
								printf('<div><button class="upvote %s" value="up"><i class="far fa-chevron-up"></i></button></div><div class="votes">%s</div><div><button class="downvote %s" value="down"><i class="far fa-chevron-down"></i></button></div>', $class, $upvotes - $downvotes, $class);
							?>
							<meta itemprop="url" content="<?php echo get_permalink(); ?>#div-comment-27">
							<meta itemprop="downvoteCount" content="<?php echo $downvotes; ?>">
							<meta itemprop="upvoteCount" content="<?php echo $upvotes; ?>">
						</form>
					</div>
					<?php endif; ?>
					<div class="best_answer">
						<?php if($checked && $best_answer) : ?>
							<i class="fas fa-check-circle"></i>
						<?php endif; ?>
					</div>
				</div>

				<div class="col">
					<footer class="comment-meta">
                        <?php
                        $user_id = $comment->user_id;
                        $avatar_size = 40;
                        $datetime = get_comment_time('c');
                        $published_date = sprintf(' %1$s %2$s', get_comment_date('d.m.Y', $comment), get_comment_time('H:i'));
                        $rating = get_comment_meta( get_comment_ID(), 'rating-' . get_the_ID(), true );
                        include get_template_directory() . '/templates/info_block.php';
                        ?>
						<?php if ( '0' == $comment->comment_approved ) : ?>
						<p class="comment-awaiting-moderation"><?php _e( '(Awaiting moderation)', 'imedix' ); ?></p>
						<?php endif; ?>
					</footer><!-- .comment-meta -->

					<div class="comment-content"<?php echo (is_category_post( 'qa' )) ? ' itemprop="text"' : ''; ?>>
						<?php comment_text(); ?>
					</div><!-- .comment-content -->

					<?php if( is_category_post( 'qa' ) ) : ?>
						<?php
							$user = wp_get_current_user();
							$allowed_roles = array('editor', 'administrator', 'author');
						?>
						<?php if( array_intersect($allowed_roles, $user->roles ) && '0' != $comment->comment_approved && $depth === 1 && ! $checked ) :  ?>
						<form action="" class="get_best_answer">
							<div class="row get_best_answer-row">
								<div class="col d-flex justify-content-end align-items-center">
									<div class="question">
										Do you find answer in your question?
									</div>
									<?php wp_nonce_field('ajax-best-comment', 'security'); ?>
									<input type="hidden" name="post_id" value="<?php the_ID(); ?>">
									<input type="hidden" name="comment_id" value="<?php comment_ID(); ?>">
									<button class="yes_btn" value="yes">
										YES
									</button>
									<button class="no_btn" value="no">
										NO
									</button>
								</div>
							</div>
						</form>
						<?php endif; ?>
					<?php endif; ?>

					<?php if( $depth < 2 && comments_open() ) : ?>
						<?php
							$id = uniqid('cm_');
						?>
						<div class="collapse_link_wrapper">
							<a href="#<?php echo $id; ?>" class="collapse_link" data-toggle="collapse">
								<span><?php _e( 'Reply', 'imedix' ); ?></span>
								<img src="<?php echo get_template_directory_uri(); ?>/production/images/reply_icon.png"
								     srcset="<?php echo get_template_directory_uri(); ?>/production/images/reply_icon@2x.png 2x,
								             <?php echo get_template_directory_uri(); ?>/production/images/reply_icon@3x.png 3x"
								     class="reply_icon">
							</a>
						</div>
						<div class="collapse" id="<?php echo $id; ?>">
							<?php display_comment_form( get_comment_ID() ); ?>
						</div>
					<?php endif; ?>

				</div>

			</div>
		</article><!-- .comment-body -->
<?php
}


function save_comment_rating( $comment_id ) {
    if(empty($_POST['comment_post_ID']) || empty($_POST['rating'])) return;

    $post_id = $_POST['comment_post_ID'];
    $rating = $_POST['rating'];

    $rating_count = get_post_meta( $post_id, 'rating_count', true );
    $rating_sum = get_post_meta( $post_id, 'rating_sum', true );
    if(empty($rating_count)) $rating_count = 0;
    if(empty($rating_sum)) $rating_sum = 0;

    $rating_count++;
    $rating_sum = $rating_sum + $rating;

    update_post_meta( $post_id, 'rating_count', $rating_count );
    update_post_meta( $post_id, 'rating_sum', $rating_sum );

    update_comment_meta( $comment_id, 'rating-' . $post_id, $rating );
}
add_action( 'comment_post', 'save_comment_rating' );
