<?php
function get_custom_single_template($single_template)
{
    global $post;

    if (is_category_post('drugs')) {
        $single_template = locate_template('single-drugs.php');
    } else if (is_category_post('qa')) {
        $single_template = locate_template('single-questions.php');
    } else if (is_category_post('pharmacy-reviews')) {
        $single_template = locate_template('single-pharmacies.php');
    }

    return $single_template;
}
add_filter('single_template', 'get_custom_single_template');

function get_custom_page_template()
{
    global $post;
    global $wp_query;

    if (!is_user_logged_in() && is_needed_page_by_slug('settings')) {

        $wp_query->set_404();
        status_header(404);
        get_template_part(404);
        exit;
    }
}
add_action('template_redirect', 'get_custom_page_template');

function get_admin_template() {
    global $post;
    global $wp_query;

    $user = wp_get_current_user();
    $allowed_roles = array('editor', 'administrator');
    if (!array_intersect($allowed_roles, $user->roles)) {

        $wp_query->set_404();
        status_header(404);
        get_template_part(404);
        exit;
    }
}
add_action('admin_menu', 'get_admin_template');

function allow_pending_posts_for_authors($query) {
    if (!$query->is_single()) return;

    $q = $query->query_vars;
    $post = get_post($q['p']);
    if ($post == null) return;
    $post_author = $post->post_author;
    $current_user = get_current_user_id();

    if ($post_author == $current_user) {
        $query->set('post_status', array('publish','pending'));
    }
}
add_action('pre_get_posts','allow_pending_posts_for_authors');

function get_user_on_users_page()
{
    add_rewrite_rule('^users/user/([^/]*)/page/?([0-9]{1,})/?', 'index.php?pagename=users/user&userid=$matches[1]&paged=$matches[2]', 'top');
    add_rewrite_rule('^users/user/([^/]*)/?', 'index.php?pagename=users/user&userid=$matches[1]', 'top');

}
add_action('init', 'get_user_on_users_page');

function query_var_userid($vars)
{
    $vars[] = 'userid';
    return $vars;
}
add_filter('query_vars', 'query_var_userid');
