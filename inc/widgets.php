<?php

/**
 * Adding widgets.
 */
require get_template_directory() . '/inc/widgets/top_categories.php';
require get_template_directory() . '/inc/widgets/top_users.php';
require get_template_directory() . '/inc/widgets/top_reviews.php';
require get_template_directory() . '/inc/widgets/top_drugs.php';
require get_template_directory() . '/inc/widgets/top_pharmacies.php';
require get_template_directory() . '/inc/widgets/relevant_drugs.php';
require get_template_directory() . '/inc/widgets/related_questions.php';
require get_template_directory() . '/inc/widgets/liability_disclaimer.php';
require get_template_directory() . '/inc/widgets/content_banner.php';
require get_template_directory() . '/inc/widgets/interacting_drugs.php';
