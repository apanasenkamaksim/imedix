<?php

function theme_initialization() {
	if ( ! empty( $_GET['theme_initialization'] ) && $_GET['theme_initialization'] == 1 ) {
		set_first_rating();
		set_user_statistic();
		create_notification_database_table();
	}
}
add_action( 'init', 'theme_initialization' );

add_action( 'switch_theme', 'create_notification_database_table' );

function set_first_rating() {
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => -1
	);
	$loop = new WP_Query( $args );

	if ($loop->have_posts()) {
		while ($loop->have_posts()) {
			$loop->the_post();
			set_rating();
		}
		wp_reset_query();
	}
}
add_action( 'switch_theme', 'set_first_rating' );



function set_user_statistic() {
	$args = array(
		'role__in' => array( 'administrator', 'editor', 'author', 'subscriber' ),
		'number' => -1
	);
	$users = new WP_User_Query( $args );

	foreach( $users->get_results() as $user ) {
		set_questions_count( $user->ID );
		set_answers_count( $user->ID );
		set_comments_count( $user->ID );
		set_upvotes_count( $user->ID );
		set_downvotes_count( $user->ID );
        set_following_count( $user->ID );
        set_followers_count( $user->ID );
		update_user_score( $user->ID );
	}
}
add_action( 'switch_theme', 'set_user_statistic' );

function set_questions_count( $user_id ) {
	$args = array(
		'category_name' => 'qa',
		'post_type' => 'post',
		'post_status' => 'any',
		'fields' => 'ids',
		'author' => $user_id,
		'posts_per_page' => -1
	);
	$loop = new WP_Query( $args );
	$questions_count = $loop->found_posts;
	update_user_meta( $user_id, 'questions_count', $questions_count );
}

function set_answers_count( $user_id ) {
	$args = array(
		'parent__in' => array( 0 ),
		'author__in' => array( $user_id ),
		'status' => 'all',
		'number' => '',
		'count' => true
	);
	$loop = new WP_Comment_Query;
	$answers_count = $loop->query( $args );
	update_user_meta( $user_id, 'answers_count', $answers_count );
}

function set_comments_count( $user_id ) {
	$args = array(
		'parent__not_in' => array( 0 ),
		'author__in' => array( $user_id ),
		'status' => 'all',
		'number' => '',
		'count' => true
	);
	$loop = new WP_Comment_Query;
	$comments_count = $loop->query( $args );
	update_user_meta( $user_id, 'comments_count', $comments_count );
}

function set_upvotes_count( $user_id ) {
	global $wpdb;

	$query = $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE user_id = %d AND meta_key LIKE 'comment_upvote_%';", $user_id );
	$upvotes_count = $wpdb->get_var( $query );
	update_user_meta( $user_id, 'upvotes_count', $upvotes_count );
}

function set_downvotes_count( $user_id ) {
	global $wpdb;

	$query = $wpdb->prepare( "SELECT COUNT(*) FROM $wpdb->usermeta WHERE user_id = %d AND meta_key LIKE 'comment_downvote_%';", $user_id );
	$downvotes_count = $wpdb->get_var( $query );
	update_user_meta( $user_id, 'downvotes_count', $downvotes_count );
}

function set_following_count( $user_id ) {
    $users = get_usermeta_values( $user_id, 'following' );
    $found_meta = $users->found_meta;

    update_user_meta( $user_id, 'following_count', $found_meta );
}

function set_followers_count( $user_id ) {
    $users = get_usermeta_values( $user_id, 'followers' );
    $found_meta = $users->found_meta;

    update_user_meta( $user_id, 'followers_count', $found_meta );
}
