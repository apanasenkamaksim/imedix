<?php get_header(); ?>
<?php 
	$args = array(
		'numberposts'	=> 1,
		'comment_status' => 'open', 
		'post_type'		=> 'doctor_post',
		'meta_key'		=> 'doctor_id',
		'meta_value'	=> $user->ID
	);
	$the_query = new WP_Query( $args );
?>
<?php if(!$the_query->have_posts() ): ?>
	<?php 
		$profile_page = wp_insert_post(array(
			'post_title'        => "$user->display_name Profile",
			'post_type'         => "doctor_post",
			'post_status'       => 'publish',
			'comment_status' 		=> 'open', 
			'post_author'       => $user->ID
		));
		if(!is_wp_error($profile_page)) add_post_meta($profile_page, 'doctor_id', $user->ID, true);
	?>
<?php endif; ?>
<?php wp_reset_query(); ?>
<main>
	<div class="container">

		<ol class="list-unstyled" id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
		  <li class="item" itemprop="itemListElement" itemscope
		      itemtype="http://schema.org/ListItem">
		    <a itemprop="item" href="<?php echo home_url(); ?>">
		    <span itemprop="name">HOME</span></a>
		    <meta itemprop="position" content="1" />
		  </li>
		  <li class="item" itemprop="itemListElement" itemscope
		      itemtype="http://schema.org/ListItem">
		    <a itemprop="item" href="<?php echo home_url( '/users/' ); ?>">
		    <span itemprop="name">USERS</span></a>
		    <meta itemprop="position" content="2" />
		  </li>
		</ol>

		<div class="row">
			<div class="col-lg-9">

				<article id="article-pharmacy">

					<?php
						$user_id = get_query_var( 'userid' );
					?>
					<?php if ( $user_id ) : ?>
						<?php
							$user = get_user_by( 'ID', $user_id );
							if (in_array( 'doctor', $user->roles, true)):
						?>
							<div class="user-info" itemscope itemtype="http://schema.org/Physician">
								<meta itemprop="url" content="<?php echo get_author_posts_url($user->ID); ?>" />
								<div class="user-block">
									<div class="text-center">
										<div class="user_thumbnail">
											<?php echo get_avatar( $user->user_email, '120', '', '', array('extra_attr'=>'itemprop="image"')); ?>
										</div>
									</div>
									<?php
										$user_position = get_field( 'position', 'user_'. $user->ID );
										$doctor_address = get_field( 'doctor_address', 'user_'. $user->ID );
										$doctor_price_range = get_field( 'doctor_price_range', 'user_'. $user->ID );
										$doctor_telephone = get_field( 'doctor_telephone', 'user_'. $user->ID );
										$doctor_info = get_field( 'doctor_info', 'user_'. $user->ID );
									?>
									<?php if ( $user_position ) : ?>
										<div class="text-center">
											<div class="position">
												<?php echo $user_position; ?>
											</div>
										</div>
									<?php endif; ?>
									<div class="text-center">
										<div class="user-block__name" itemprop="name">
											<?php echo $user->display_name; ?>

											<?php if ( get_current_user_id() == $user->ID ) : ?>
											<a href="<?php echo home_url('/settings/account/'); ?>" class="edit_link">Edit</a>
											<?php endif; ?>
										</div>
									</div>
									<div class="text-center">
										<?php
											$website = $user->user_url;
											$facebook = get_field( 'facebook', 'user_'. $user->ID );
											$instagram = get_field( 'instagram', 'user_'. $user->ID );
											$linkedin = get_field( 'linkedin', 'user_'. $user->ID );
											$twitter = get_field( 'twitter', 'user_'. $user->ID );
											$youtube = get_field( 'youtube', 'user_'. $user->ID );
											$wikipedia = get_field( 'wikipedia', 'user_'. $user->ID );
										?>
										<?php if ($website): ?>
											<a href="<?php echo $website; ?>" class="user-info__social-icon social-icon_website" target="_blank"><i class="fa fa-globe" aria-hidden="true"></i></a>
										<?php endif; ?>
										<?php if ($facebook): ?>
											<a href="<?php echo $facebook; ?>" class="user-info__social-icon user-info__social-icon_facebook" target="_blank"><i class="fab fa-facebook" aria-hidden="true"></i></a>
										<?php endif; ?>
										<?php if ($instagram): ?>
											<a href="<?php echo $instagram; ?>" class="user-info__social-icon user-info__social-icon_instagram" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a>
										<?php endif; ?>
										<?php if ($linkedin): ?>
											<a href="<?php echo $linkedin; ?>" class="user-info__social-icon user-info__social-icon_linkedin" target="_blank"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
										<?php endif; ?>
										<?php if ($twitter): ?>
											<a href="<?php echo $twitter; ?>" class="user-info__social-icon user-info__social-icon_twitter" target="_blank"><i class="fab fa-twitter" aria-hidden="true"></i></a>
										<?php endif; ?>
										<?php if ($youtube): ?>
											<a href="<?php echo $youtube; ?>" class="user-info__social-icon user-info__social-icon_youtube" target="_blank"><i class="fab fa-youtube" aria-hidden="true"></i></a>
										<?php endif; ?>
										<?php if ($wikipedia): ?>
											<a href="<?php echo $wikipedia; ?>" class="user-info__social-icon user-info__social-icon_wikipedia" target="_blank"><i class="fab fa-wikipedia-w" aria-hidden="true"></i></a>
										<?php endif; ?>
									</div>
									<div class="information priceRange text-center" style="margin-bottom: 10px;">
										<span>
											Price Range: <span itemprop="priceRange"><?php echo $doctor_price_range; ?></span>
										</span>
									</div>
									<div class="information address text-center" style="margin-bottom: 10px;">
										<span>
											Address: <span itemprop="address"><?php echo $doctor_address; ?></span>
										</span>
									</div>
									<div class="information telephone text-center" style="margin-bottom: 10px;">
										<span>
											Phone: <span itemprop="telephone"><?php echo $doctor_telephone; ?></span>
										</span>
									</div>
									<div class="information text-center">
										<?php 
											$args = array(
												'numberposts'	=> 1,
												'comment_status' => 'open', 
												'post_type'		=> 'doctor_post',
												'meta_key'		=> 'doctor_id',
												'meta_value'	=> $user->ID
											);
											$the_query = new WP_Query( $args );										
											if( $the_query->have_posts() ): 
										?>
											<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
												<?php
												
												$comments = get_comments('post_id='.get_the_ID().'&status=approve');
												$rating_default = !empty($verdict['rating']) ? $verdict['rating'] : 4;
												$rating_count = 0;
												$rating_sum = 0;
												
												foreach($comments as $key=>$comment){
													$rating = get_comment_meta($comment->comment_ID, 'rating-' . get_the_ID(), true);
													if (!$rating && $comment->comment_parent==0) $rating = $rating_default;
													if ($rating && $comment->comment_parent==0) {
														$rating_sum += $rating;
														$rating_count++;
													}
												}
																																		
												if ($rating_sum==0) $rating_sum = $rating_default;
												if ($rating_count==0) $rating_count = 1;
																																		
												$rating_value = !empty($rating_count) && !empty($rating_sum) ? round($rating_sum / $rating_count, 2) : $rating_default;
												
												?>
												<?php display_rating($rating_value); ?>	
												<div style="margin-top: 5px;">
													Rating: <span><?php echo $rating_value; ?></span>
												</div>
											<?php endwhile; ?>
										<?php endif; ?>
										<?php wp_reset_query(); ?>	
									</div>
								</div>							
								<?php
									$filter = (! empty( $_GET['filter'] )) ? $_GET['filter'] : false;
								?>
								<div class="tabs_btns tabs_btns_js">
									<ul class="list-unstyled panel-list">
										<li class="item">
											<a href="#info" class="item_link item_link__js<?php echo (empty($_GET['filter'])) ? ' active' : ''; ?>">
												Info
											</a>
										</li>
										<li class="item">
											<a href="#reviews" class="item_link item_link__js">
												Reviews
											</a>
										</li>
										<li class="item">
											<a href="<?php echo get_author_posts_url($user->ID) . '/?filter=questions'; ?>" class="item_link <?php if ( $filter == 'questions' ) echo 'active'; ?>">
												Questions
											</a>
										</li>
										<li class="item">
											<a href="<?php echo get_author_posts_url($user->ID) . '/?filter=answers-comments'; ?>" class="item_link <?php if ( $filter == 'answers-comments' ) echo 'active'; ?>">
												Answers & Comments
											</a>
										</li>
									</ul>
								</div>
								<div id="article-any" class="tabs_content">
									<div id="info" class="tabs-block<?php echo (empty($_GET['filter'])) ? ' active' : ''; ?>">
										<div itemprop="description">
											<?php echo $doctor_info; ?>
										</div>
									</div>
									<div id="reviews" class="tabs-block">
										<?php 
											$args = array(
												'numberposts'	=> 1,
												'comment_status' => 'open', 
												'post_type'		=> 'doctor_post',
												'meta_key'		=> 'doctor_id',
												'meta_value'	=> $user->ID
											);
											$the_query = new WP_Query( $args );										
											if( $the_query->have_posts() ): 
										?>
											<?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
												<?php comments_template(); ?>
											<?php endwhile; ?>
										<?php endif; ?>
										<?php wp_reset_query(); ?>
									</div>
									<div id="questions" class="tabs-block<?php if ( $filter == 'questions' ) echo ' active'; ?>">
										<?php
											$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
											$temp_wp_query = $wp_query;
										?>
										<?php if ( $filter == 'questions' ) : ?>
											<?php

																			if (is_user_logged_in()) {
																					function filter_where_status_front_page($where) {
																							$where .= " OR (post_author = " . get_current_user_id() . " AND
																									post_status = 'pending'
																							) ";
																							return $where;
																					}
																					add_filter('posts_where', 'filter_where_status_front_page');
																			}

												$options = array(
													'paged' => $paged,
													'orderby' => array ('date' => 'DESC'),
													// 'posts_per_page' => 2,
													'category_name' => 'qa',
													'post_type' => 'post',
													'author' => $user_id,
													'post_status' => array('publish')

												);
												$wp_query = new WP_Query( $options );
												$total = $wp_query->max_num_pages;

																			if (is_user_logged_in()) remove_filter('posts_where', 'filter_where_status');
											?>

											<?php if ( have_posts() ) : ?>

												<div id="articles">

												<?php while (have_posts()) : the_post(); ?>

													<?php include get_template_directory() . '/templates/article/article-any.php'; ?>

												<?php endwhile; wp_reset_query(); ?>

												</div>

											<?php else : ?>

													<div class="user-post">
																							User didn't post got any question.
																					</div>

											<?php endif; ?>

											<?php
												$paginate = paginate_links(array(
														// 'total' => 8,
														// 'current' => 1,
														'total' => $total,
														'current' => $paged,
														// 'base' => URI . '/my-account/manage-tours/' . '%_%',
														// 'format' => '%#%',
														'type' => 'array',
														'end_size' => 1,
														'mid_size' => 1,
														'prev_text' => '<i class="fas fa-caret-left"></i>',
														'next_text' => '<i class="fas fa-caret-right"></i>',
												));
												// die(var_dump($paginate));
												display_pagination( $paginate );

												$wp_query = $temp_wp_query;
											?>
										<?php endif; ?>
									</div>
									<div id="answers-comments" class="tabs-block<?php if ( $filter == 'answers-comments' ) echo ' active'; ?>">
										<?php if ( $filter == 'answers-comments' ) : ?>
											<?php
												$args = array(
													'author__in' => array( $user->ID ),
													'status' => 'all',
													'count' => true
												);
												$loop = new WP_Comment_Query;
												$total = $loop->query( $args );
												$args['number'] = 10;
												$total = ceil( $total / $args['number'] );
												$args['paged'] = $paged;
												$args['count'] = false;
												$loop = new WP_Comment_Query( $args );
												$comments = $loop->comments;
												// d( $total );
											?>

											<?php if ( count( $comments ) >= 1 ) : ?>
											<div id="articles" class="white">
											<?php foreach( $comments as $comment ) : ?>

													<div class="row m-0">
														<div class="col p-0">

															<?php include get_template_directory() . '/templates/article/article-comment.php'; ?>

														</div>
													</div>

											<?php endforeach; ?>
											</div>
											<?php endif; ?>

											<?php if ( count( $comments ) < 1 ) : ?>

																	<div class="user-post">
																			User didn't post any answers and comments.
																	</div>

											<?php endif; ?>

											<?php
												$paginate = paginate_links(array(
														// 'total' => 8,
														// 'current' => 1,
														'total' => $total,
														'current' => $paged,
														// 'base' => URI . '/my-account/manage-tours/' . '%_%',
														// 'format' => '%#%',
														'type' => 'array',
														'end_size' => 1,
														'mid_size' => 1,
														'prev_text' => '<i class="fas fa-caret-left"></i>',
														'next_text' => '<i class="fas fa-caret-right"></i>',
												));
												// die(var_dump($paginate));
												display_pagination( $paginate );

												$wp_query = $temp_wp_query;
											?>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php else: ?>
							<div class="user-block">
								<div class="text-center">
									<div class="user_thumbnail">
										<?php echo get_avatar( $user->user_email, '120' ); ?>
									</div>
								</div>
								<?php
									$user_position = get_field( 'position', 'user_'. $user->ID );
								?>
								<?php if ( $user_position ) : ?>
									<div class="text-center">
										<div class="position">
											<?php echo $user_position; ?>
										</div>
									</div>
								<?php endif; ?>
								<div class="text-center" itemscope itemtype="http://schema.org/Person">
									<div class="user-block__name" itemprop="name">
										<?php echo $user->display_name; ?>

										<?php if ( get_current_user_id() == $user->ID ) : ?>
										<a href="<?php echo home_url('/settings/account/'); ?>" class="edit_link">Edit</a>
										<?php endif; ?>
									</div>
								</div>
								<?php
									$score = (int) get_user_meta( $user->ID, 'user_score', true );
									$followers_count = (int) get_user_meta( $user->ID, 'followers_count', true );
									$questions_count = (int) get_user_meta( $user->ID, 'questions_count', true );
									$answers_count = (int) get_user_meta( $user->ID, 'answers_count', true );
								?>
								<div class="information text-center">
									<span>
										Score: <span><?php echo $score; ?></span>
									</span>
									<span>
										Folowers: <span><?php echo $followers_count; ?></span>
									</span>
									<span>
										Questions: <span><?php echo $questions_count; ?></span>
									</span>
									<span>
										Answers: <span><?php echo $answers_count; ?></span>
									</span>
								</div>
								<div class="text-center">
								<?php display_favorites_button( 'user', $user->ID ); ?>
								</div>
							</div>

							<?php
								$filter = ( ! empty( $_GET['filter'] ) && $_GET['filter'] == 'answers-comments' ) ? 'answers-comments' : 'questions';
							?>
							<div class="tabs_btns">
								<ul class="list-unstyled panel-list">
									<li class="item">
										<a href="<?php echo get_author_posts_url($user->ID) . '/?filter=questions'; ?>" class="item_link <?php if ( $filter == 'questions' ) echo 'active'; ?>">
											Questions
										</a>
									</li>
									<li class="item">
										<a href="<?php echo get_author_posts_url($user->ID) . '/?filter=answers-comments'; ?>" class="item_link <?php if ( $filter == 'answers-comments' ) echo 'active'; ?>">
											Answers & Comments
										</a>
									</li>
								</ul>
							</div>

							<?php
								$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
								$temp_wp_query = $wp_query;
							?>
							<?php if ( $filter == 'questions' ) : ?>
							<?php

															if (is_user_logged_in()) {
																	function filter_where_status_front_page($where) {
																			$where .= " OR (post_author = " . get_current_user_id() . " AND
																					post_status = 'pending'
																			) ";
																			return $where;
																	}
																	add_filter('posts_where', 'filter_where_status_front_page');
															}

								$options = array(
									'paged' => $paged,
									'orderby' => array ('date' => 'DESC'),
									// 'posts_per_page' => 2,
									'category_name' => 'qa',
									'post_type' => 'post',
									'author' => $user_id,
									'post_status' => array('publish')

								);
								$wp_query = new WP_Query( $options );
								$total = $wp_query->max_num_pages;

															if (is_user_logged_in()) remove_filter('posts_where', 'filter_where_status');
							?>

							<?php if ( have_posts() ) : ?>

								<div id="articles">

								<?php while (have_posts()) : the_post(); ?>

									<?php include get_template_directory() . '/templates/article/article-any.php'; ?>

								<?php endwhile; wp_reset_query(); ?>

								</div>

							<?php else : ?>

									<div class="user-post">
																			User didn't post got any question.
																	</div>

							<?php endif; ?>
							<?php else : ?>

							<?php
								$args = array(
									'author__in' => array( $user->ID ),
									'status' => 'all',
									'count' => true
								);
								$loop = new WP_Comment_Query;
								$total = $loop->query( $args );
								$args['number'] = 10;
								$total = ceil( $total / $args['number'] );
								$args['paged'] = $paged;
								$args['count'] = false;
								$loop = new WP_Comment_Query( $args );
								$comments = $loop->comments;
								// d( $total );
							?>

							<?php if ( count( $comments ) >= 1 ) : ?>
							<div id="articles" class="white">
							<?php foreach( $comments as $comment ) : ?>

									<div class="row m-0">
										<div class="col p-0">

											<?php include get_template_directory() . '/templates/article/article-comment.php'; ?>

										</div>
									</div>

							<?php endforeach; ?>
							</div>
							<?php endif; ?>

							<?php if ( count( $comments ) < 1 ) : ?>

													<div class="user-post">
															User didn't post any answers and comments.
													</div>

							<?php endif; ?>

							<?php endif; ?>

							<?php
								$paginate = paginate_links(array(
										// 'total' => 8,
										// 'current' => 1,
										'total' => $total,
										'current' => $paged,
										// 'base' => URI . '/my-account/manage-tours/' . '%_%',
										// 'format' => '%#%',
										'type' => 'array',
										'end_size' => 1,
										'mid_size' => 1,
										'prev_text' => '<i class="fas fa-caret-left"></i>',
										'next_text' => '<i class="fas fa-caret-right"></i>',
								));
								// die(var_dump($paginate));
								display_pagination( $paginate );

								$wp_query = $temp_wp_query;
							?>
						<?php endif; ?>
					<?php endif; ?>
				</article>
			</div>
			<aside class="col-lg-3 sidebar-col d-none d-lg-block">
				<?php
					if ( ! dynamic_sidebar('user_post_sidebar') ) _e('Add widgets to sidebar', 'imedix');
				?>
			</aside>
		</div>
	</div>
</main>

<?php get_footer(); ?>
