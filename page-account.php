<?php get_header(); ?>

<main>
	<div class="container">
		<div class="row">
			<aside class="col-lg-3 sidebar-settings d-none d-lg-block">

				<?php include get_template_directory() . '/templates/settings_sidebar.php'; ?>

			</aside>
			<div class="col-lg-9">
				<div class="content-settings account">
                    <h1 class="page_title">
                        Account Settings
                    </h1>
                    <form action="" id="ajax_account_settings" novalidate>
                        <?php
                        $user = wp_get_current_user();
                        ?>
                        <div class="selected_avatar" style="background-image: url('<?php echo get_avatar_url( $user->user_email, '64' ); ?>');"></div>
                        <a href="javascript:;" class="change_avatar_link">
                            <input type="file" class="change_avatar">
                            Change Avatar
                        </a>

                        <div class="row">
                            <div class="col-12 input-col">
                                <label>
                                    <i>*</i> <?php _e('First Name', 'imedix'); ?>
                                </label>
                                <input type="text" name="name" class="name" placeholder="John" data-empty="Name is Required" data-wrong="Wrong Name" value="<?php echo $user->user_firstname ; ?>">
                            </div>
                            <div class="col-12 input-col last">
                                <label>
                                    <i>*</i> <?php _e('Last Name', 'imedix'); ?>
                                </label>
                                <input type="text" name="lastname" class="lastname" placeholder="Johnson" data-empty="Lastname is Required" data-wrong="Wrong Lastname" value="<?php echo $user->user_lastname ; ?>">
                            </div>

                            <div class="col-12">
                                <hr>
                                <a href="#" class="change_password_popup_link">Change Password</a>
                                <hr>
                            </div>

                            <div class="col-12 page_title">
                                Connected Accounts
                            </div>
                            <div class="col-12 social-col">
                                <div class="row">
                                    <div class="col">
                                        <img src="<?php echo get_template_directory_uri(); ?>/production/images/logo-facebook.png"
                                             srcset="<?php echo get_template_directory_uri(); ?>/production/images/logo-facebook@2x.png 2x,
											           <?php echo get_template_directory_uri(); ?>/production/images/logo-facebook@3x.png 3x"
                                             class="google_logo"> Facebook

                                    </div>
                                    <div class="col">
                                        <?php
                                        if ( isCurrentUserConnected( 'fb' ) ) {
                                            $action = 'unlink';
                                            $class = '';
                                            $text = 'Disconnect';
                                        } else {
                                            $action = 'link';
                                            $class = 'active';
                                            $text = 'Connect';
                                        }
                                        ?>
                                        <a href="<?php echo home_url('/wp-login.php?loginSocial=facebook&action=' . $action . '&redirect=' . urlencode(current_url())); ?>" data-plugin="nsl" data-action="<?php echo $action; ?>" redirect="<?php echo current_url(); ?>" data-provider="facebook" data-popupwidth="475" data-popupheight="175" class="social_link <?php echo $class; ?>">
                                            <?php echo $text; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 social-col last">
                                <div class="row">
                                    <div class="col">
                                        <img src="<?php echo get_template_directory_uri(); ?>/production/images/logo-google-2.png"
                                             srcset="<?php echo get_template_directory_uri(); ?>/production/images/logo-google-2@2x.png 2x,
									             <?php echo get_template_directory_uri(); ?>/production/images/logo-google-2@3x.png 3x"
                                             class="google_logo"> Google
                                    </div>
                                    <div class="col">
                                        <?php
                                        if ( isCurrentUserConnected( 'google' ) ) {
                                            $action = 'unlink';
                                            $class = '';
                                            $text = 'Disconnect';
                                        } else {
                                            $action = 'link';
                                            $class = 'active';
                                            $text = 'Connect';
                                        }
                                        ?>
                                        <a href="<?php echo home_url('/wp-login.php?loginSocial=google&action=' . $action . '&redirect=' . urlencode(current_url())); ?>" data-plugin="nsl" data-action="<?php echo $action; ?>" redirect="<?php echo current_url(); ?>" data-provider="google" data-popupwidth="600" data-popupheight="600" class="social_link <?php echo $class; ?>">
                                            <?php echo $text; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <?php stl_wp_nonce_field('ajax-account-settings', 'security'); ?>
                                <input type="submit" class="blue_btn" value="Save">
                            </div>
                        </div>
                    </form>

				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
