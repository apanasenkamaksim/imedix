<?php get_header(); ?>

<main>
    <div class="container">

        <ol class="list-unstyled" id="breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li class="item" itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo home_url(); ?>">
                    <span itemprop="name">HOME</span></a>
                <meta itemprop="position" content="1"/>
            </li>
            <li class="item" itemprop="itemListElement" itemscope
                itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="<?php echo home_url('/category/drugs/'); ?>">
                    <span itemprop="name">DRUGS</span></a>
                <meta itemprop="position" content="2"/>
            </li>
        </ol>

        <div class="row">
            <div class="col-lg-9">

                <?php if(have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>

                        <article id="article-any" itemid="<?php the_permalink(); ?>#drug" itemscope itemtype="http://schema.org/Drug">

                            <div>
                                <h1 id="article_hedline" itemprop="alternateName"><?php the_title(); ?></h1>

                                <?php $thumbnail_url = get_the_post_thumbnail_url(); ?>
                                <?php if($thumbnail_url) : ?>
                                    <meta itemprop="image" content="<?php echo $thumbnail_url; ?>">
                                <?php endif; ?>

                                <div class="row align-items-center">
                                    <div class="row m-0">
                                        <div>
                                            <div class="col">
                                                <?php
																								
																								$comments = get_comments('post_id='.get_the_ID().'&status=approve');
																								$rating_default = !empty($verdict['rating']) ? $verdict['rating'] : 4;
																								$rating_count = 0;
																								$rating_sum = 0;
																								
																								foreach($comments as $key=>$comment){
																									$rating = get_comment_meta($comment->comment_ID, 'rating-' . get_the_ID(), true);
																									if (!$rating && $comment->comment_parent==0) $rating = $rating_default;
																									if ($rating && $comment->comment_parent==0) {
																										$rating_sum += $rating;
																										$rating_count++;
																									}
																								}
																																														
																								if ($rating_sum==0) $rating_sum = $rating_default;
																								if ($rating_count==0) $rating_count = 1;
																																														
																								$rating_value = !empty($rating_count) && !empty($rating_sum) ? round($rating_sum / $rating_count, 2) : $rating_default;
																								
                                                ?>
                                                <?php display_rating($rating_value); ?>
                                                <div class="rating_info">
                                                    (
                                                    <span><?php echo $rating_value; ?></span>/
                                                    <span>5</span>
                                                    )
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <?php $verdict = get_field('verdict'); ?>
                                                <div class="verdict_wrapper drugs">
                                                    <div class="title" itemprop="name">
                                                        <?php echo $verdict['title']; ?>
                                                    </div>
                                                    <div class="row">
                                                        <?php if(has_post_thumbnail()) : ?>
                                                            <div class="col-md-4 thumbnail-col">
                                                                <div class="article_thumbnail">
                                                                    <?php the_post_thumbnail('medium'); ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        <div class="col right-col">
                                                            <div class="text" itemprop="description">
                                                                <?php echo $verdict['text']; ?>
                                                            </div>
                                                            <?php if(!empty($verdict['active_ingredient'])) : ?>
                                                                <div class="price"><b>Active Ingredient:</b> <span
                                                                            itemprop="activeIngredient"><?php echo $verdict['active_ingredient']; ?></span>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if(!empty($verdict['dosage_form'])) : ?>
                                                                <div class="price"><b>Dosage Form:</b> <span
                                                                            itemprop="dosageForm"><?php echo $verdict['dosage_form']; ?></span>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if(!empty($verdict['dosage'])) : ?>
                                                                <div class="price"><b>Dosage:</b>
																																<?php $dosages = explode('; ', $verdict['dosage']); ?>
																																<?php foreach($dosages as $dosage): ?>
																																	<span itemprop="drugUnit"><?php echo $dosage; ?><?php echo !empty($verdict['dosage_form']) ? ' '.$verdict['dosage_form'] : ''; ?></span>;
																																<?php endforeach; ?>
                                                                </div>
                                                            <?php endif; ?>
                                                            <?php if(!empty($verdict['price'])) : ?>
                                                                <div class="price"><b>Minimum Market Price:</b> <span
                                                                            itemprop="cost">$<?php echo $verdict['price']; ?></span>
                                                                </div>
                                                            <?php endif; ?>
                                                            <div class="text-right">
                                                                <a class="blue_btn" href="<?php echo $verdict['link']; ?>"
                                                                   target="_blank">
                                                                    OPEN STORE
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 text-col">

                                        <?php the_content(); ?>

                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-12 widgets-col">
                                    <?php
                                    if(!dynamic_sidebar('drugs_post_content'))
                                        _e('Add widgets to sidebar', 'imedix');
                                    ?>
                                </div>
                                <div class="col-12">
																	<div class="verdict_wrapper drugs">
																			<div class="title">
																					<?php echo $verdict['title']; ?>
																			</div>
																			<div class="row">
																					<?php if(has_post_thumbnail()) : ?>
																							<div class="col-md-4 thumbnail-col">
																									<div class="article_thumbnail">
																											<?php the_post_thumbnail('medium'); ?>
																									</div>
																							</div>
																					<?php endif; ?>
																					<div class="col right-col">
																							<div class="text">
																									<?php echo $verdict['text']; ?>
																							</div>
																							<?php if(!empty($verdict['active_ingredient'])) : ?>
																									<div class="price"><b>Active Ingredient:</b> <span><?php echo $verdict['active_ingredient']; ?></span>
																									</div>
																							<?php endif; ?>
																							<?php if(!empty($verdict['dosage_form'])) : ?>
																									<div class="price"><b>Dosage Form:</b> <span><?php echo $verdict['dosage_form']; ?></span>
																									</div>
																							<?php endif; ?>
																							<?php if(!empty($verdict['dosage'])) : ?>
																									<div class="price"><b>Dosage:</b>
																									<?php $dosages = explode('; ', $verdict['dosage']); ?>
																									<?php foreach($dosages as $dosage): ?>
																										<span><?php echo $dosage; ?><?php echo !empty($verdict['dosage_form']) ? ' '.$verdict['dosage_form'] : ''; ?></span>;
																									<?php endforeach; ?>
																									</div>
																							<?php endif; ?>
																							<?php if(!empty($verdict['price'])) : ?>
																									<div class="price"><b>Minimum Market Price:</b> <span>$<?php echo $verdict['price']; ?></span>
																									</div>
																							<?php endif; ?>
																							<div class="text-right">
																									<a class="blue_btn" href="<?php echo $verdict['link']; ?>"
																										 target="_blank">
																											OPEN STORE
																									</a>
																							</div>
																					</div>
																			</div>
																	</div>				
                                </div>
                            </div>

                            <div class="row popularity-col">
                                <div class="col item">
                                    <img src="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon.png"
                                         srcset="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon@2x.png 2x,
										 <?php echo get_template_directory_uri(); ?>/production/images/comments_icon@3x.png 3x"
                                         class="comments_image">
                                    <span class="text">
								<span class="description">Review: </span><?php echo $rating_count; ?>
							</span>
                                </div>
                            </div>

                            <?php comments_template(); ?>
                        </article>

                    <?php endwhile;
                    wp_reset_query(); ?>
                <?php endif; ?>

            </div>
            <aside class="col-lg-3 sidebar-col d-none d-lg-block">
                <?php
                if(!dynamic_sidebar('drugs_post_sidebar'))
                    _e('Add widgets to sidebar', 'imedix');
                ?>
            </aside>
        </div>
    </div>
</main>
<?php get_footer(); ?>
