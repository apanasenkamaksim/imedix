jQuery(document).ready(function($) {

	$(document).on('click', '.delete_question_link', function() {
    $('#remove_question_popup .remove_question_btn').attr('href', $(this).attr('href'));
    $('#remove_question_popup .remove_question_btn').attr('data-nonce', $(this).attr('data-nonce'));
    $('#remove_question_popup .remove_question_btn').attr('data-is_single', $(this).attr('data-is_single'));
    $('#remove_question_popup .remove_question_btn').attr('data-redirect', $(this).attr('data-redirect'));

		$.fancybox.open({
			src : '#remove_question_popup'
		});

		return false;
	});

	$(document).on('click', '.remove_question_link', function() {

  	let link = $(this);
    let form_data = [];

    let is_single = $(this).attr('data-is_single');
    let redirect = $(this).attr('data-redirect');

    form_data.push({name: 'post_id', value: $(this).attr('href')});
    form_data.push({name: 'nonce', value: $(this).attr('data-nonce')});
    form_data.push({name: 'action', value: 'ajax_remove_question'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					$('#remove_question_popup .message').text(response.message);
					if(is_single) {
						location.replace(redirect);
					} else {
						$('article.post-id-' + response.post_id).remove();
					}
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

		return false;
	});


});
