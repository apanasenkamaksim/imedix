jQuery(document).ready(function($) {

	$(document).on('click', '.remove_user_from_favorites_link', function() {

  	let link = $(this);
  	let ID = $(this).attr('href');
    let form_data = [];

    form_data.push({name: 'user_id', value: ID});
    form_data.push({name: 'nonce', value: $(this).attr('data-nonce')});
    form_data.push({name: 'action', value: 'ajax_remove_user_from_favorites'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					$('.following .item-col .remove_user_from_favorites_link[href="' + ID + '"]').each(function() {
						let item = $(this).closest('.item-col');
						item.remove();
					});
					$('.favorites_small_button.remove_user_from_favorites_link[href="' + ID + '"]').replaceWith(response.favorites_small_button);
					$('.favorites_button.remove_user_from_favorites_link[href="' + ID + '"]').replaceWith(response.favorites_button);
					$('.favorites_menu .remove_user_from_favorites_link[href="' + ID + '"]').each(function() {
						let favorites_menu = $(this).closest('.favorites_menu');
						favorites_menu.replaceWith(response.favorites_menu);
					});
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

		return false;
	});


});
