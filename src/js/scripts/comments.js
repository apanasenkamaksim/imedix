jQuery(document).ready(function($) {

	define(['autosize'], function (autosize) {
	  autosize($('.comment-form textarea'));
	});

	$('.comment .collapse_link').click(function(){
    let text = $(this).find('span').first().text();
		$(this).find('span').text(text == "Reply" ? "Collapse" : "Reply");
	});

  $('.get_best_answer button').click(function() {

  	let button = $(this);
  	let form = button.closest('.get_best_answer');
    let form_data = form.serializeArray();

    form_data.push({name: 'action', value: 'ajax_best_comment'});
    form_data.push({name: 'submit', value: button.val()});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
      	if(response.state == true) {
					if(button.val() == 'yes') {
						$(button.closest('.comment-body').find('.best_answer').html('<i class="fas fa-check-circle"></i>'));
						$('.get_best_answer').remove();
					} else {
						form.remove();
					}
      	}

        // $('.tours_count').html(response.count);
        // form_data = form_data.slice(0,form_data.length-1);
        // alert($.param(form_data));

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

  $('.set_votes button.disabled').click(function() {
		$.fancybox.open(
			'<div class="message">You must be logged in to give your vote.</div>',
			{
				smallBtn : true,
				toolbar : false
			}
		);
  });
  $('.set_votes button.enabled').click(function() {

  	let button = $(this);
  	let form = button.closest('.set_votes');
    let form_data = form.serializeArray();

    form_data.push({name: 'action', value: 'ajax_comment_votes'});
    form_data.push({name: 'submit', value: button.val()});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
      	if(response.state == true) {
					form.find('.votes').text(response.votes);
      	} else if(response.message != 'undefind') {
					$.fancybox.open(
						'<div class="message">' + response.message + '</div>',
						{
							smallBtn : true,
							toolbar : false
						}
					);
      	}

        // $('.tours_count').html(response.count);
        // form_data = form_data.slice(0,form_data.length-1);
        // alert($.param(form_data));

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

});
