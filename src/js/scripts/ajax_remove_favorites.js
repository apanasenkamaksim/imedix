jQuery(document).ready(function($) {

	$('.remove_favorites_link').click(function() {

  	let link = $(this);
    let form_data = [];

    form_data.push({name: 'category_ID', value: $(this).attr('href')});
    form_data.push({name: 'nonce', value: $(this).attr('data-nonce')});
    form_data.push({name: 'action', value: 'ajax_remove_favorites'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					link.closest('.item-col').remove();
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

		return false;
	});


});
