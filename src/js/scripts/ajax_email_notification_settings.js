jQuery(document).ready(function($) {

  $('#ajax_email_notification_settings').submit(function() {
  	let form = $(this);

    let form_data = $(this).serializeArray();

    form_data.push({name: 'action', value: 'ajax_email_notification_settings'});

    $.ajax({
      type: 'post',
      url: theme.ajaxurl,
      dataType: 'json',
      data: form_data,
      beforeSend: function () {

      },
      success: function (response) {
				if (response.state == true) {
					location.reload();
        } else {
        	alert(response.message);
        }

      },
      error: function (response) {
        console.log(response);
      }
    });

  	return false;
  });

});
