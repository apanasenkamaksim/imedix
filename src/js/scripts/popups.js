jQuery(document).ready(function ($) {

    $('.close_popups_btn').click(function () {
        $.fancybox.close(true);
    });

    $.extend($.fancybox.defaults, {
        touch: false,
        fullScreen: false,
        keyboard: false,
        btnTpl: {
            smallBtn:
            '<button data-fancybox-close class="fancybox-close-small" title="{{CLOSE}}">' +
            '<i class="far fa-times"></i>' +
            '</button>'
        },
        beforeShow: function (instance, current) {
            $('#main_menu_wrapper').collapse('hide');
            $('#users_menu').collapse('hide');
            $('#events_menu').collapse('hide');
            $('.question_menu').collapse('hide');
        }
    });

    $('.login_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '#login_popup'
        });

        return false;
    });

    $('.registration_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '#registration_popup'
        });

        return false;
    });

    $('.restore_password_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '#restore_password_popup'
        });

        return false;
    });

    $('.change_password_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '#new_password_popup'
        });

        return false;
    });

    $('.question_editor_popup_link').click(function () {
        $.fancybox.close(true);

        if ($('#question_editor_popup input[name="post_id"]').val() != 0) {
            $('#question_editor_popup .title').text('Add question');
            $('#question_editor_popup input[name="post_id"]').val(0);
            $('#question_editor_popup .title_input').val('');
            $('#question_editor_popup #question_textarea').summernote('code', '');
            $('#question_editor_popup .category_input').each(function () {
                if ($(this).prop('checked')) {
                    $(this).trigger('click');
                }
            });
        }
        $.fancybox.open({
            src: '#question_editor_popup',
            opts: {
                afterShow: function (instance, current) {
                    let object = $('.multiselect');
                    let length = 0;
                    object.find('.item').each(function () {
                        length = length + $(this).outerWidth(true);
                    });
                    let display = object.find('.display').first();
                    let search = object.find('.search').first();
                    let width = display.width();
                    if (length / width <= 1) {
                        width = width - length;
                    } else {
                        width = length % width;
                    }
                    search.css({'width': '50px'});
                    search.width(Math.max(width - search.css('padding-left').replace("px", "") - search.css('padding-right').replace("px", ""), search.width()));
                }
            }
        });

        return false;
    });

    if ($.cookie('cookies_allowed') != 'true') {
        $.fancybox.open({
            src: '#cookies_popup',
            opts: {
                baseClass: 'bg-no',
                hideScrollbar: false,
                afterClose: function () {
                    $.cookie('cookies_allowed', 'true', {expires: 7, path: '/'});
                },
                beforeShow: function (instance, current) {
                    $('#cookies_popup').css({top: top_header_height()});
                }
            }
        });
    }

    $(window).on('scroll resize', function () {
        $('#cookies_popup').css({top: top_header_height()});
    });
});


