jQuery(document).ready(function($) {

	$('#testimonials-carousel').owlCarousel({
      // loop:true,
      loop:true,
      margin:0,
      dots:false,
      nav:true,
      navText: [
                  '<i class="far fa-angle-left"></i>',
                  '<i class="far fa-angle-right"></i>'
              ],
      items:1
	});

	$('#media-carousel').owlCarousel({
      loop:true,
      margin:0,
      dots:false,
      nav:true,
      navText: [
                  '<i class="far fa-angle-left"></i>',
                  '<i class="far fa-angle-right"></i>'
              ],
      items:1
	});

});
