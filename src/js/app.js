import $ from 'jquery';
import Popper from 'popper.js';

import '@fancyapps/fancybox/dist/jquery.fancybox.min';

import 'owl.carousel/src/js/owl.carousel';
import 'owl.carousel/src/js/owl.navigation';

import 'summernote/dist/summernote-bs4';

import 'jquery.cookie';

// import 'rangeslider.js/dist/rangeslider';

import 'autosize';

import 'bootstrap';

// import 'bootstrap/js/src/util';
// import 'bootstrap/js/src/alert';
// import 'bootstrap/js/src/button';
// import 'bootstrap/js/src/carousel';
// import 'bootstrap/js/src/collapse';
// import 'bootstrap/js/src/dropdown';
// import 'bootstrap/js/src/index';
// import 'bootstrap/js/src/modal';
// import 'bootstrap/js/src/popover';
// import 'bootstrap/js/src/scrollspy';
// import 'bootstrap/js/src/tab';
// import 'bootstrap/js/src/tooltip';

//Custom

window.viewport = function () {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {width: e[a + 'Width'], height: e[a + 'Height']};
};
window.top_header_height = function() {
    let height = 0;

    if(viewport().width <= 600 && $(document).scrollTop() == 0) {
		height = $('#wpadminbar').length > 0 ? $('#wpadminbar').height() : 0;
    } else if(viewport().width > 600) {
        height = $('#wpadminbar').length > 0 ? $('#wpadminbar').height() : 0;
    }

    height += $('#navigation').height();

	return height;
};

import './scripts/close_menus';
import './scripts/main_menu';
import './scripts/searchform';
import './scripts/popups';
import './scripts/ajax_login';
import './scripts/ajax_registration';
import './scripts/ajax_email_confirmation';
import './scripts/ajax_restore_password';
import './scripts/ajax_new_password';
import './scripts/ajax_question_editor';
import './scripts/ajax_get_question';
import './scripts/ajax_remove_question';
import './scripts/ajax_edit_comment';
import './scripts/ajax_add_question_to_favorites';
import './scripts/ajax_add_user_to_favorites';
import './scripts/ajax_remove_question_from_favorites';
import './scripts/ajax_remove_user_from_favorites';
import './scripts/ajax_account_settings';
import './scripts/ajax_email_notification_settings';
import './scripts/ajax_read_notifications';
import './scripts/comments';
import './scripts/multiselect';


jQuery(document).ready(function($) {

	// scrollToElement($('#navigation a'), true);


  // Multiselect
  if($.isFunction($.fn.multiselect)){
    $('.multiselect').multiselect('init');
  }

	$("#ajax_question_editor #question_textarea").summernote({
	  toolbar: [
	    ['style', ['bold', 'italic', 'underline', 'clear']],
	    ['font', ['strikethrough']],
	    ['fontsize', ['fontsize']],
	    ['color', ['color']],
	    ['para', ['ul', 'ol', 'paragraph']],
	    ['height', ['height']]
	  ],
	  height: 120,
		popover: {
			image: [],
			link: [],
			air: []
		}
	});

	function readURL(input) {
		if (input.files && input.files[0]) {
			let reader = new FileReader();

			reader.onload = function (e) {
				$('.selected_avatar').css({'background-image': 'url("' + e.target.result + '")'});
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$('.change_avatar').change(function() {
		readURL(this);
	});

});


// Scroll to element
function scrollToElement(elem, toFrontPage = false) {

	elem.on('click', function(e) {

		let section = $($(this).attr('href'));
		if (section.length === 0 && toFrontPage) {
			window.location.href = theme.url + $(this).attr('href');
			e.preventDefault();
		} else if (section.length > 0) {
			let wpadminbar_height = ($('#wpadminbar').length == 1) ? $('#wpadminbar').height() : 0;
			if ($(document).scrollTop() <= section.offset().top) {
				$('html,body').animate({ 'scrollTop': section.offset().top - wpadminbar_height }, 500, 'swing');
			}

			// history.pushState("", document.title, window.location.pathname);
			window.location.href.substr(0, window.location.href.indexOf('#'));
			e.preventDefault();
		}
	})
}
