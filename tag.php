<?php get_header(); ?>

<?php if( is_front_page() && ! is_user_logged_in() ) : ?>
<?php include get_template_directory() . '/templates/header_section_front_page.php'; ?>
<?php endif; ?>

<main>
	<div class="container">
		<div class="row">
			<div class="col-lg-9">

				<?php
                if ( !is_search() && !is_404() ) {
                    include get_template_directory() . '/templates/articles_headline.php';
                }
                ?>
				<?php
						include get_template_directory() . '/templates/articles/articles-tag.php';
				?>

			</div>
			<aside class="col-lg-3 sidebar-col d-none d-lg-block">
			</aside>
		</div>
	</div>
</main>

<?php get_footer(); ?>
