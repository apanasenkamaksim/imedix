<?php get_header(); ?>

<main>
	<div class="container">
		<div class="row">
			<aside class="col-lg-3 sidebar-settings d-none d-lg-block">

				<?php include get_template_directory() . '/templates/settings_sidebar.php'; ?>

			</aside>
			<div class="col-lg-9">
				<div class="content-settings following">
					<div class="content">
						<?php
							$users = get_usermeta_values( get_current_user_id(), 'following', 30 );
							$total = $users->max_num_pages;
							$found_meta = $users->found_meta;
							$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						?>
						<h1 class="page_title">
							<?php echo $found_meta; ?> Following
						</h1>
						<div class="row">
						<?php $count = count( $users->meta ); ?>
						<?php $i = 1; foreach( $users->meta as $meta ) : ?>
							<?php $user = get_user_by( 'ID', $meta->meta_value  ); ?>
							<div class="col-12 col-sm-6 item-col">


								<div class="open_favorites_menu left">
									<div class="item">
										<a href="<?php echo get_author_posts_url($user->ID); ?>" class="item_link">
											<div class="user_thumbnail">
												<?php echo get_avatar( $user->user_email, '24' ); ?>
											</div>
											<div class="user_name">
												<?php echo $user->display_name; ?>
											</div>
										</a>
										<?php display_favorites_small_button( 'user', $user->ID ); ?>
									</div>
									<?php display_favorites_menu( 'user', $user->ID ); ?>
								</div>


							</div>
						<?php $i++; endforeach; ?>
						</div>
					</div>
					<div class="row">
						<div class="col">
							<?php
							 $paginate = paginate_links(array(
									// 'total' => 8,
									// 'current' => 1,
							     'total' => $total,
							     'current' => $paged,
							     // 'base' => URI . '/my-account/manage-tours/' . '%_%',
							     // 'format' => '%#%',
							     'type' => 'array',
							     'end_size' => 1,
							     'mid_size' => 1,
							     'prev_text' => '<i class="fas fa-caret-left"></i>',
							     'next_text' => '<i class="fas fa-caret-right"></i>',
							 ));
							 // die(var_dump($paginate));
							display_pagination( $paginate );
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>
