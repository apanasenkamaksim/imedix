<div class="col-12">

	<article class="post-id-<?php echo the_ID(); ?> article-question_or_drugs">

		<?php include get_template_directory() . '/templates/question_menu.php'; ?>

		<div class="row content-row">
			<?php if(has_post_thumbnail()) : ?>
			<div class="col-12 col-sm-4 order-sm-3 thumbnail-col">
				<?php the_post_thumbnail( 'medium', array( 'class' => 'article_image' ) ); ?>
			</div>
			<?php endif; ?>
			<div class="col-12">

				<?php include get_template_directory() . '/templates/article_tags.php'; ?>

			</div>
			<div class="col-12 title-col">
				<h3 class="title">
					<?php the_title(); ?>
				</h3>
				<?php
					$user_id = get_the_author_meta('ID');
					$avatar_size = 40;
					$datetime = get_the_date('Y-m-d\TH:i');;
					$published_date = get_the_date( 'd.m.Y H:i' );
					include get_template_directory() . '/templates/info_block.php';
				?>
			</div>
			<div class="col-12 col-sm-8 order-sm-4 text-col">
				<?php
					include get_template_directory() . '/templates/info_block.php';
				?>
				<div class="text">
					<?php echo_excerpt( 400, '' ); ?>
				</div>
			</div>
		</div>
		<div class="row align-items-center bottom-row">
			<div class="col">
				<div class="row">
					<div class="col-auto item">
						<img src="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon.png"
							 srcset="<?php echo get_template_directory_uri(); ?>/production/images/comments_icon@2x.png 2x,
									 <?php echo get_template_directory_uri(); ?>/production/images/comments_icon@3x.png 3x"
							 class="comments_image">
						<span class="text">
							<span class="description">Respond: </span><?php echo get_comments_number(); ?>
						</span>
					</div>
					<div class="col-auto item">
						<img src="<?php echo get_template_directory_uri(); ?>/production/images/views_icon.png"
							 srcset="<?php echo get_template_directory_uri(); ?>/production/images/views_icon@2x.png 2x,
									 <?php echo get_template_directory_uri(); ?>/production/images/views_icon@3x.png 3x"
							 class="comments_image">
						<span class="text">
							<span class="description">Views: </span><?php display_views(); ?>
						</span>
					</div>
				</div>
			</div>
			<div class="col-auto">
				<a href="<?php the_permalink(); ?>" class="blue_btn">
					View more
				</a>
			</div>
		</div>
	</article>

</div>
