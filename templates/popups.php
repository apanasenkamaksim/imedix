<?php
function get_popups() {
	/**
	 * Adding popups.
	 */

	require get_template_directory() . '/templates/popups/new_password.php';
	require get_template_directory() . '/templates/popups/cookies.php';

	if( is_user_logged_in() ) {
		require get_template_directory() . '/templates/popups/question_editor.php';
		require get_template_directory() . '/templates/popups/remove_question.php';
	} else {
		require get_template_directory() . '/templates/popups/login.php';
		require get_template_directory() . '/templates/popups/registration.php';
		require get_template_directory() . '/templates/popups/confirm_email.php';
		require get_template_directory() . '/templates/popups/email_confirmed.php';
		require get_template_directory() . '/templates/popups/restore_password.php';
	}
}
add_action('wp_footer', 'get_popups');
