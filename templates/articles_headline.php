<?php 
	//$is_needed_category_qa = is_needed_category( 'qa' );
	$is_needed_category_qa = true;
	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
?>
<div id="articles_headline">
	<div class="row">
		<?php if ($paged==1): ?>
			<div class="<?php echo is_needed_category( 'pharmacy-reviews' ) || is_needed_category( 'drugs' ) ? 'col-md-12' : 'col-md-6'; ?> title-col">
				<h2 class="title">
					<?php
					if ( is_front_page() ) {
						echo 'Blog';
					} elseif ( is_category( 'qa' ) ) {
						echo 'Top Questions';
					} elseif ( is_category( 'pharmacy-reviews' ) || is_category( 'drugs' ) ) {
						single_cat_title();
					} elseif ( is_category() ) {
						echo 'Category: ';
						single_cat_title();
					} elseif ( is_page() ) {
						the_title();
					}
					?>
				</h2>
				<?php if ( $category_description = category_description() ) : ?>
				<div class="description">
					<?php echo $category_description; ?>
				</div>
				<?php endif; ?>
				<?php
					$category = get_queried_object();
				?>
				<?php
					if ( !is_category( 'qa' ) && $is_needed_category_qa ) {
						display_favorites_button( 'category', $category->term_id );
					}
				?>
			</div>
		<?php endif; ?>
        <?php if ( $is_needed_category_qa ) : ?>
        <div class="col-md-6">
			<div class="row justify-content-end">
				<div class="col-auto">
					<?php $class = is_user_logged_in() ? 'question_editor_popup_link' : 'login_popup_link'; ?>
					<a class="orange_btn <?php echo $class; ?>">
						<i class="fal fa-plus"></i> add Question
					</a>
				</div>
			</div>
			<?php
				//$filters = array( 'Interesting', 'Top Week', 'Top Month', 'All' );
				$filters = array( 'Interesting', 'All' );
				$filters_slugs = array_map( 'strtolower', $filters );
				$filters_slugs = str_replace( ' ', '-', $filters_slugs );
				$filter = ! empty( $_GET[ 'filter' ] ) && in_array( $_GET[ 'filter' ], $filters_slugs ) ? $_GET[ 'filter' ] : 'all';
			?>
			<!--<div class="row">
				<div class="col">
					<ul class="list-unstyled panel-list">
						<?php foreach ($filters as $key => $value) : ?>
						<li class="item">
							<a href="<?php echo add_query_arg( array( 'filter' => $filters_slugs[ $key ] ) ); ?>" class="item_link<?php echo $filter == $filters_slugs[ $key ] ? ' active' : ''; ?>">
								<?php echo $value; ?>
							</a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
			</div>-->
		</div>
        <?php endif; ?>
    </div>
</div>
