<div id="articles" <?php if ( is_needed_page_by_slug( 'users' ) ) echo 'class="white"'; ?>>

<?php
	if( empty( $filter ) ) $filter = 'all';

	if ( $filter == 'top-week' ) {
			function filter_where_date($where = '') {
		    //posts in the last 7 days
		    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-7 days')) . "'";
		    return $where;
			}
			add_filter('posts_where', 'filter_where_date');
	}

	if ( $filter == 'top-month' ) {
		function filter_where_date($where = '') {
	    //posts in the last 30 days
	    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
	    return $where;
		}
		add_filter('posts_where', 'filter_where_date');
	}

	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$options['role__in'] = array( 'subscriber' );
	$options['number'] = 40;
	$options['paged'] = $paged;


//	if ( $filter == 'interesting' || $filter == 'top-week' || $filter == 'top-month' ) {
        //$options['orderby'] = array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' );
        //$options['meta_key'] = 'user_score';
//	}
	// d( $options );
	$loop = new WP_User_Query( $options );
	$all = $loop->get_total();
	$total = ceil( $all / $options['number'] );
	// echo $paged.'<br>';
	// echo $all.'<br>';
	// echo $total;

	if ( $filter == 'top-week' || $filter == 'top-month' ) remove_filter( 'posts_where', 'filter_where_date' );
?>

<?php $count = count( $loop->get_results() ); ?>
<?php $i = 1; foreach( $loop->get_results() as $user ) : ?>

	<?php if ( $i % 2 == 1 ) : ?>
	<div class="row m-0">
	<?php endif; ?>

		<div class="col-md-6">

		<?php include get_template_directory() . '/templates/article/article-user.php'; ?>

		</div>

	<?php if ( $i % 2 == 0 || $i == $count ) : ?>
	</div>
	<?php endif; ?>

<?php $i++; endforeach; ?>

</div>

<?php
  $paginate = paginate_links(array(
			// 'total' => 8,
			// 'current' => 1,
      'total' => $total,
      'current' => $paged,
      // 'base' => URI . '/my-account/manage-tours/' . '%_%',
      // 'format' => '%#%',
      'type' => 'array',
      'end_size' => 1,
      'mid_size' => 1,
      'prev_text' => '<i class="fas fa-caret-left"></i>',
      'next_text' => '<i class="fas fa-caret-right"></i>',
  ));
  // die(var_dump($paginate));
	display_pagination( $paginate );
?>
