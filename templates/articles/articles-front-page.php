<div id="articles" <?php if ( is_needed_page_by_slug( 'users' ) ) echo 'class="white"'; ?>>

<?php
	if( empty( $filter ) ) $filter = 'all';

	if ( $filter == 'top-week' ) {
			function filter_where_date($where = '') {
		    //posts in the last 7 days
		    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-7 days')) . "'";
		    return $where;
			}
			add_filter('posts_where', 'filter_where_date');
	}

	if ( $filter == 'top-month' ) {
		function filter_where_date($where = '') {
	    //posts in the last 30 days
	    $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
	    return $where;
		}
		add_filter('posts_where', 'filter_where_date');
	}

	if ( is_user_logged_in() && is_needed_category( 'qa' ) ) {
		function filter_where_status_category( $where ) {
			global $wpdb;
	    $where .= " OR (post_author = " . get_current_user_id() . " AND
	    	post_status = 'pending' AND ID IN (SELECT object_id FROM " . $wpdb->prefix . "term_relationships WHERE term_taxonomy_id = '" . get_current_category_id() . "')
	    ) ";
	    return $where;
		}
		add_filter('posts_where', 'filter_where_status_category');
	}
	if ( is_user_logged_in() ) {
		function filter_where_status_front_page( $where ) {
	    $where .= " OR (post_author = " . get_current_user_id() . " AND
	    	post_status = 'pending'
	    ) ";
	    return $where;
		}
		add_filter('posts_where', 'filter_where_status_front_page');
	}

	$options = array(
		'orderby' => array ( 'date' => 'DESC' ),
		'posts_per_page' => 10,
	);

	$paged = get_query_var('page') ? get_query_var('page') : 1;
	$options['category_name'] = 'blog,qa';
	$options['post_type'] = 'post';
	$options['paged'] = $paged;

	if ( $filter == 'interesting' || $filter == 'top-week' || $filter == 'top-month' ) {
		$options['orderby'] = array ( 'meta_value_num' => 'DESC', 'date' => 'DESC' );
		$options['meta_key'] = 'post_rating';
	}
	// d( $options );
    $loop = new WP_Query( $options );
    $all = $loop->found_posts;
    $total = $loop->max_num_pages;
	// echo $paged.'<br>';
	// echo $all.'<br>';
	// echo $total;

	if ( $filter == 'top-week' || $filter == 'top-month' ) remove_filter( 'posts_where', 'filter_where_date' );
	if ( is_user_logged_in() && is_needed_category( 'qa' ) ) remove_filter( 'posts_where', 'filter_where_status' );

?>

<?php if ( $loop->have_posts() ) : ?>

	<div class="row">

		<?php $i = 1; while ($loop->have_posts()) : $loop->the_post(); ?>
		<?php
			include get_template_directory() . '/templates/article/article-any.php';
		?>

		<?php if ( $i == 2 || $i == 5 ) : ?>
			<div class="col-12 widgets-col">
				<?php
					$sidebar = 'front_page_content';
					$sidebar .= $i == 2 ? '' :'_2';
					if ( empty( $sidebar ) || ! dynamic_sidebar( $sidebar ) )
						_e('Add widgets to sidebar', 'imedix');
				?>
			</div>
		<?php endif; ?>

		<?php $i++; endwhile; wp_reset_query(); ?>

	</div>

<?php endif; ?>

</div>

<?php
  $paginate = paginate_links(array(
			// 'total' => 8,
			// 'current' => 1,
      'total' => $total,
      'current' => $paged,
      // 'base' => URI . '/my-account/manage-tours/' . '%_%',
      // 'format' => '%#%',
      'type' => 'array',
      'end_size' => 1,
      'mid_size' => 1,
      'prev_text' => '<i class="fas fa-caret-left"></i>',
      'next_text' => '<i class="fas fa-caret-right"></i>',
  ));
  // die(var_dump($paginate));
	//display_pagination( $paginate );
?>
