<div id="articles" <?php if ( is_needed_page_by_slug( 'users' ) ) echo 'class="white"'; ?>>
<?php
	$tag = get_query_var( 'tag' );

	$options = array(
		'orderby' => array ( 'date' => 'DESC' ),
		// 'posts_per_page' => 2,
	);


	$paged = get_query_var('paged') ? get_query_var('paged') : 1;
	$options['post_type'] = 'post';
	$options['tag'] = $tag;

	$options['paged'] = $paged;


	$loop = new WP_Query( $options );
	$all = $loop->found_posts;
	$total = $loop->max_num_pages;

?>

<?php if ( ( !is_page() || is_front_page() ) && $loop->have_posts()) : ?>

	<div class="row">

		<?php $i = 1; while ($loop->have_posts()) : $loop->the_post(); ?>
		<?php
			if ( is_category_post( 'pharmacy-reviews' ) ) {
				include get_template_directory() . '/templates/article/article-pharmacy.php';
			} else {
				include get_template_directory() . '/templates/article/article-any.php';
			}
		?>

		<?php if ( ( is_front_page() || is_needed_category( 'qa' ) ) && ( $i == 2 || $i == 5 ) ) : ?>
			<div class="col-12 widgets-col">
				<?php
					if ( is_front_page() ) {
						$sidebar = 'front_page_content';
					} else if ( is_needed_category( 'qa' ) ) {
						$sidebar = 'question_post_content';
					} else if ( is_needed_category( 'drugs' ) ) {
						$sidebar = 'drugs_post_content';
					}
					if ( empty( $sidebar ) || ! dynamic_sidebar( $sidebar ) )
						_e('Add widgets to sidebar', 'imedix');
				?>
			</div>
		<?php endif; ?>

		<?php $i++; endwhile; wp_reset_query(); ?>

	</div>

<?php else : ?>

	<?php $count = count( $loop->get_results() ); ?>
	<?php $i = 1; foreach( $loop->get_results() as $user ) : ?>

		<?php if ( $i % 2 == 1 ) : ?>
		<div class="row m-0">
		<?php endif; ?>

			<div class="col-md-6">

			<?php include get_template_directory() . '/templates/article/article-user.php'; ?>

			</div>

		<?php if ( $i % 2 == 0 || $i == $count ) : ?>
		</div>
		<?php endif; ?>

	<?php $i++; endforeach; ?>

<?php endif; ?>

</div>

<?php
  $paginate = paginate_links(array(
			// 'total' => 8,
			// 'current' => 1,
      'total' => $total,
      'current' => $paged,
      // 'base' => URI . '/my-account/manage-tours/' . '%_%',
      // 'format' => '%#%',
      'type' => 'array',
      'end_size' => 1,
      'mid_size' => 1,
      'prev_text' => '<i class="fas fa-caret-left"></i>',
      'next_text' => '<i class="fas fa-caret-right"></i>',
  ));
  // die(var_dump($paginate));
	display_pagination( $paginate );
?>
