<?php /**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 12/13/2018
 * Time: 10:42 PM
 */
?>

<?php $verdict = get_field( 'verdict'); ?>
<?php if( $verdict[ 'positive_or_not' ] ) : ?>
<div class="store-fixed">
    <div class="container">
        <div class="row align-items-center bottom-row">

            <div class="col-6">
                <a href="<?php echo $verdict[ 'link_to_store' ]; ?>" class="blue_btn store-btn" target="_blank"> open store </a>
            </div>

            <?php if( $verdict[ 'discount_state' ] ) : ?>
            <div class="col-6">
                <div class="store-coupon">
                    <div class="store-title-coupon">discount coupon</div>
                    <div class="store-des-coupon"><?php echo $verdict[ 'discount' ]; ?>% off</div>
                </div>
            </div>
            <?php endif; ?>

        </div>
    </div>
</div>
<?php endif; ?>