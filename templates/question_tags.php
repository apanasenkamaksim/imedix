<ul class="list-unstyled tags-list">
	<li class="item">
		<a href="#" class="item_link blue">
			QA
		</a>
	</li>
	<?php if( ! comments_open() || post_has_the_best_comment() ) : ?>
	<li class="item">
		<a href="#" class="item_link red">
			Closed
		</a>
	</li>
	<?php endif; ?>
	<?php
		$categories = wp_get_post_categories( get_the_ID(), array('fields' => 'all') );
	?>
	<?php foreach ($categories as $category) : ?>
	<li class="item">
		<a href="<?php echo get_category_link( $category->term_id ); ?>" class="item_link white">
			<?php echo $category->name ?>
		</a>
	</li>
<?php endforeach; ?>
</ul>
<div class="article_status">
	<?php if( $post->post_status == 'pending' ) : ?>
	(Awaiting moderation)
	<?php endif; ?>
</div>
