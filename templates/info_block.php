<?php
	// Needs
	// $user_id
	// $avatar_size
	// $datetime
	// $published_date
    // $rating

	if ( empty( $avatar_size ) ) $avatar_size = 40;
	if (!$user_id) $user_id = get_the_author_ID();
	if (!$published_date) $published_date = get_the_date();
	$user = get_user_by( 'id', $user_id );
	// d( $user );
?>

<div class="info-block">
	<a class="user_thumbnail" href="<?php echo get_author_posts_url($user_id); ?>">
		<?php echo get_avatar( $user->user_email, $avatar_size ); ?>
	</a>
	<div class="data">
		<div class="author">
		
			<?php if (is_category_post( 'qa' )): ?>
				<a class="name" itemprop="author" itemscope itemtype="https://schema.org/Person" href="<?php echo get_author_posts_url($user_id); ?>>">
					<span itemprop="name">
						<?php echo $user->display_name; ?>
					</span>
				</a>
			<?php else: ?>
				<a class="name" href="<?php echo get_author_posts_url($user_id); ?>">
					<span>
						<?php echo $user->display_name; ?>
					</span>
				</a>
			<?php endif; ?>
			<?php
				$user_position = get_field( 'position', 'user_'. $user_id );
			?>
			<?php if ( $user_position ) : ?>
			<div class="position">
				<?php echo $user_position; ?>
			</div>
			<?php endif; ?>
		</div>
		<?php if (!is_category_post( 'qa' )): ?>
        <?php if(!empty($rating) && $depth==1) : ?>
        <?php display_rating($rating, true); ?>
        <?php elseif(empty($rating) && $depth==1): ?>
        <?php display_rating(4, true); ?>
        <?php endif; ?>
		<?php endif; ?>
			
		<div class="date">
			<?php
				_e('Published at', 'imedix');
				printf( ' %1$s', $published_date);
			?>
		</div>
		<?php if (is_category_post( 'qa' )): ?>
			<?php if ($comment->comment_date): ?>
				<meta itemprop="dateCreated" content="<?php echo date( 'c', strtotime($comment->comment_date)); ?>">
				<meta itemprop="dateModified" content="<?php echo date( 'c', strtotime($comment->comment_date)); ?>">
			<?php else: ?>
				<meta itemprop="dateCreated" content="<?php echo date( 'c', strtotime(get_the_date())); ?>">
			<?php endif; ?>
		<?php else: ?>
			<meta itemprop="dateCreated" content="<?php echo date( 'c', strtotime(get_the_date())); ?>">
		<?php endif; ?>
	</div>
</div>
