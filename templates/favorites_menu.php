<?php
	// Needs
	// $ID
	// $add_favorites
	// $link_class
	// $description
	// $data_nonce
	// $count_text
	// $count
?>
<div class="favorites_menu">

	<div class="content">

		<ul class="mnu_el">

			<li class="mnu_top_wr">
				<a class="top-wr-el <?php echo $link_class; ?>" href="<?php echo $ID; ?>" data-nonce="<?php echo $data_nonce; ?>">
					<div class="ico_wr">
						<?php if ( $add_favorites ) : ?>
						<i class="far fa-plus"></i>
						<?php else : ?>
						<i class="far fa-minus"></i>
						<?php endif; ?>
					</div>
					<div class="top_title"><?php echo $add_favorites ? 'Add to' : 'Remove from'; ?> favorites</div>
				</a>
			</li>

			<?php if ( ! empty( $description ) ) : ?>
			<li class="mnu_text_wr">
			<?php echo $description; ?>
			</li>
			<?php endif; ?>

			<li class="mnu_line"></li>

			<li class="mnu_bot_wr">
				<div class="bot_title"><?php echo $count_text; ?>:</div>
				<div class="bot_number"><?php echo $count; ?></div>
			</li>

		</ul>

	</div>

</div>
