<article class="article-empty">

	<div class="empty-el-wr">

		<div class="img-wr">
			<img src="<?php echo get_template_directory_uri(); ?>/production/images/empty_search.png"
					 srcset="<?php echo get_template_directory_uri(); ?>/production/images/empty_search@2x.png 2x,
							 <?php echo get_template_directory_uri(); ?>/production/images/empty_search@3x.png 3x"
					 class="empty_img">
		</div>

		<div class="title_bot">
			alheimer's and parkinson's not found
		</div>

		<div class="button_wr">
			<a class="orange_btn question_editor_popup_link">
				<i class="fal fa-plus"></i> add Question
			</a>
		</div>

	</div>

</article>
