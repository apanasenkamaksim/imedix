<div id="remove_question_popup" class="fancybox-content" style="display: none;">
	<div class="title">
		Confirm Removing Question
	</div>
	<div class="row">
		<div class="col-12 text-center">
			<div class="text">
				The question will be removed. Continue?
			</div>
			<a href="#" class="remove_question_link remove_question_btn">
				Yes
			</a>
			<div class="message"></div>
		</div>
	</div>
</div>
