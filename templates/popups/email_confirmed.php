<div id="email_confirmed_popup" class="fancybox-content" style="display: none;">
	<div class="title">
		Confirmation of the email was successful
	</div>
	<div class="row">
		<div class="col-12 text-center">
			<a href="#" class="close_popups_btn">
				Go to The System
			</a>
		</div>
	</div>
</div>
