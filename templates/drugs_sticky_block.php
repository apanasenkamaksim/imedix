<?php /**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 12/13/2018
 * Time: 10:41 PM
 */

?>

<?php $verdict = get_field('verdict'); ?>
<div class="store-fixed">
    <div class="container">
        <div class="row align-items-center bottom-row">

            <div class="col-6">
                <a class="blue_btn store-btn" href="<?php echo $verdict['link']; ?>" target="_blank">
                    open store
                </a>
            </div>

            <?php if(!empty($verdict['price'])) : ?>
                <div class="col-6">
                    <div class="store-price">
                        <div class="store-title-price">Minimum Market Price</div>
                        <div class="store-des-price">$<?php echo $verdict['price']; ?></div>
                    </div>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>