<?php
	$args = array(
		'menu'              => 'settings',
		'menu_id'           => 'settings_menu',
		'menu_class'        => 'list-unstyled',
		'theme_location'    => 'settings',
		'container'         => ''
	);
	wp_nav_menu( $args );
?>
