<?php get_header(); ?>

<?php if( is_front_page() && ! is_user_logged_in() ) : ?>
<?php include get_template_directory() . '/templates/header_section_front_page.php'; ?>
<?php endif; ?>

<main>
	<div class="container">
		<div class="row">
			<div class="col-lg-9">

				<?php
                if ( !is_search() && !is_404() ) {
                    include get_template_directory() . '/templates/articles_headline.php';
                }
                ?>
				<?php
					if ( is_front_page() ) {
						include get_template_directory() . '/templates/articles/articles-front-page.php';
					} else if ( is_needed_category( 'qa' ) ) {
						include get_template_directory() . '/templates/articles/articles-qa.php';
					} else if ( is_needed_category( 'drugs' ) ) {
						include get_template_directory() . '/templates/articles/articles-drugs.php';
					} else if ( is_needed_category( 'pharmacy-reviews' ) ) {
						include get_template_directory() . '/templates/articles/articles-pharmacy.php';
					} else if ( is_page( 'users' ) ) {
						include get_template_directory() . '/templates/articles/articles-users.php';
					} else if ( is_page( 'doctors' ) ) {
						include get_template_directory() . '/templates/articles/articles-doctors.php';
					} else if ( is_search() ) {
						include get_template_directory() . '/templates/articles/articles-search.php';
                    } else if ( is_404() ) {
                        include get_template_directory() . '/templates/article/article-404.php';
					} else {
						include get_template_directory() . '/templates/articles/articles.php';
					}
				?>

			</div>
			<aside class="col-lg-3 sidebar-col d-none d-lg-block">
				<?php
					if ( is_front_page() ) {
						$sidebar = 'front_page_sidebar';
					} else if ( is_needed_category( 'qa' ) ) {
						$sidebar = 'question_category_sidebar';
					} else if ( is_needed_category( 'drugs' ) ) {
						$sidebar = 'drugs_category_sidebar';
					} else if ( is_needed_category( 'pharmacy-reviews' ) ) {
						$sidebar = 'pharmacy_category_sidebar';
					} else if ( is_page( 'users' ) ) {
						$sidebar = 'user_category_sidebar';
					} else if ( is_page( 'doctors' ) ) {
						$sidebar = 'user_category_sidebar';
					} else if ( is_search() || is_404() ) {
						$sidebar = 'search_page_sidebar';
					}
					if ( !empty( $sidebar ) && ! dynamic_sidebar( $sidebar ) )
						_e('Add widgets to sidebar', 'imedix');
				?>
			</aside>
		</div>
	</div>
</main>

<?php get_footer(); ?>
