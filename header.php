<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-147144366-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-147144366-1');
</script>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" type="image/x-icon">

	<?php wp_head(); ?>

	<!--<link rel="canonical" href="<?php echo wp_get_canonical_url(); ?>" />-->
</head>

<body <?php body_class(); ?>>

<div id="site-wrapper">

	<header id="site-header">

		<nav id="navigation">
			<div class="container">
				<div class="row menu-row">
					<div class="col col-sm-6 position-static">
						<div class="row h-100">
							<div class="col-auto toggler-col">
						    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_menu_wrapper" aria-controls="main_menu_wrapper" aria-expanded="false" aria-label="Toggle navigation">
						        <i class="far fa-bars"></i>
						    </button>
						  </div>
						  <div class="col-auto align-self-center logo-col">
						  	<?php if( !is_front_page() ) : ?>
						    <a href="<?php echo home_url(); ?>" title="<?php echo get_bloginfo('name'); ?>">
						    <?php endif; ?>
									<img alt="<?php echo get_bloginfo('name'); ?>" src="<?php echo get_template_directory_uri(); ?>/production/images/logo.png"
										 srcset="<?php echo get_template_directory_uri(); ?>/production/images/logo@2x.png 2x,
												 <?php echo get_template_directory_uri(); ?>/production/images/logo@3x.png 3x"
										 class="logo">
						  	<?php if( !is_front_page() ) : ?>
								</a>
						    <?php endif; ?>
							</div>
							<div class="col position-static menu-col">
						    <div class="collapse" id="main_menu_wrapper">
						    	<div class="search">
						    		<?php get_search_form(); ?>
						    	</div>
									<?php
										$args = array(
											'menu'              => 'primary',
											'menu_id'           => 'main_menu',
											'theme_location'    => 'primary',
											// 'depth'             => 4,
											'walker'            => new Custom_Sublevel_Walker,
											'container'         => ''
										);
										wp_nav_menu( $args );
									?>
						    </div>
							</div>
						</div>
					</div>
					<div class="col-auto col-sm-6">
						<div class="row h-100 align-items-center justify-content-end">
							<div class="col search-col">
								<?php get_search_form(); ?>
								<script>
									jQuery(document).ready(function($) {
										$('.search-col input[type="text"]').attr('placeholder', '<?php _e('Search', 'imedix'); ?>');
									});
								</script>
							</div>
							<?php if( !is_user_logged_in() ) : ?>
							<div class="col-auto login-col">
								<a class="login_btn login_popup_link">
									Log in
								</a>
							</div>
							<div class="col-auto signup-col">
								<a class="signup_menu_btn active registration_popup_link">
									Sign Up
								</a>
							</div>
							<?php else : ?>
                            <?php
                                $display_count = 6;
                                $notifications = get_notifications( $display_count );
                                $unreaded_notifications = array_filter($notifications->notifications,
                                    function($notification) {
                                         return $notification->readed ? false : true;
                                    }
                                );
                                $unreaded_count = count( $unreaded_notifications );
                                $count = $notifications->found_notifications;
                            ?>
							<div class="col-auto notifications-col">
								<button data-toggle="collapse" data-target="#notifications_menu" class="notifications_toggler <?php if($unreaded_count > 0) echo ' active'; ?>">
									<i class="fas fa-bell"></i>
									<i class="fas fa-circle"></i>
								</button>
								<?php include get_template_directory() . '/templates/notifications_menu.php'; ?>
							</div>
							<div class="col-auto users-col">
								<?php
									$user = wp_get_current_user();
								?>
								<button data-toggle="collapse" data-target="#users_menu" class="users_toggler">
									<div class="user_thumbnail">
										<?php echo get_avatar( $user->user_email, '32' ); ?>
									</div>
									<div class="user_name">
										<?php echo $user->user_firstname . ' ' . $user->user_lastname; ?>
									</div>
								</button>
								<div id="users_menu" class="collapse">
									<ul class="list-unstyled users-list">
										<li class="item">
											<a href="<?php echo get_author_posts_url($user->ID); ?>" class="item_link">
												<i class="fas fa-user"></i> MY Profile
											</a>
										</li>
										<li class="item">
											<a href="<?php echo home_url( '/settings/account' ); ?>" class="item_link">
												<i class="fas fa-cog"></i> Setting
											</a>
										</li>
										<li class="item">
											<a href="<?php echo wp_logout_url(current_url()); ?>" class="item_link">
												<i class="far fa-sign-out-alt"></i> Logout
											</a>
										</li>
									</ul>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</nav>

	</header><!-- #site-content -->

	<div id="site-content">
